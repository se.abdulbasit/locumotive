package com.example.project;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import static com.example.project.DashboardActivity.setDrawer;

public class SettingActivity extends AppCompatActivity {

    TextView dash;
    TextView cancelText;
    TextView Notification;
    TextView paymentHistory;
    TextView contact;
    TextView feed;
    TextView pro;
    TextView pol;
    TextView term;
    TextView log;
    TextView del;
    TextView addAccount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        cancelText = findViewById(R.id.cancelText);
        addAccount = findViewById(R.id.addAccount);
        int c = getIntent().getIntExtra("c" , 0);
        ImageView noti =  findViewById(R.id.noti);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ChatScreenActivity.this.finish();
                startActivity(new Intent(SettingActivity.this , NotificationActivity.class));
            }
        });
        if(Who.select == 0)
        {
            DashboardActivity.setMenuClick(this);
            addAccount.setVisibility(View.GONE);
            cancelText.setVisibility(View.VISIBLE);
        }
        else
        {
            JobFeedActivity.setMenuClick(this);
            addAccount.setVisibility(View.VISIBLE);
            cancelText.setVisibility(View.GONE);
        }



        addAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this , Create_Account.class);
                intent.putExtra("key" , 3);
                startActivity(intent);
            }
        });
        contact = findViewById(R.id.contact);
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this , ContactActivity.class));
            }
        });
        feed = findViewById(R.id.feed);
        feed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this , ContactActivity.class));
            }
        });
        term = findViewById(R.id.term);
        term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this , TermAndConditions.class));
            }
        });
        pol = findViewById(R.id.pol);
        pol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this , Policy.class));
            }
        });
        pro = findViewById(R.id.pro);
        pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this , ContactActivity.class));
            }
        });
        paymentHistory = findViewById(R.id.paymentHistory);
        paymentHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this , PaymentHistory.class));
            }
        });
        dash = findViewById(R.id.dash);
        dash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Who.select == 0)
                    startActivity(new Intent(SettingActivity.this , DashboardActivity.class));
                else
                startActivity(new Intent(SettingActivity.this , JobFeedActivity.class));
                ;
            }
        });

        Notification = findViewById(R.id.Notification);
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this , NotificationActivity.class));
            }
        });


        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this , Order_Cancel.class));
            }
        });
        log = findViewById(R.id.log);
        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(SettingActivity.this);
                builder.setMessage("Are you sure you want to logout?")
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(SettingActivity.this , Who.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
                builder.show();
            }
        });
        del = findViewById(R.id.del);
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(SettingActivity.this);
                builder.setMessage("Are you sure you want to delete account?")
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(SettingActivity.this , Who.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
                builder.show();
            }
        });
        setDrawer(this);
    }
}
