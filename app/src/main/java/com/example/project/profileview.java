package com.example.project;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class profileview extends Fragment {

    TextView message;
    View v;

    public profileview() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_profileview, container, false);
        ImageView  attach = (ImageView) v.findViewById(R.id.attach);
        attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),101);
            }
        });
        message = v.findViewById(R.id.message);
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity() , ChatScreenActivity.class));
            }
        });
        TextView accept = v.findViewById(R.id.accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity() , "Locum has been approved.",Toast.LENGTH_SHORT).show();
                getActivity().finish();
        }
        });
        TextView reject = v.findViewById(R.id.reject);
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity()  , "Locum has been rejected.",Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        });
        TextView invite = v.findViewById(R.id.invite);
        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity()  , "Locum has been Invited.",Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        });
        RelativeLayout bottom = v.findViewById(R.id.bottom);
        RelativeLayout bottom2 = v.findViewById(R.id.bottom2);

        if(Who.select == 0)
        {
            bottom.setVisibility(View.GONE);
            attach.setVisibility(View.VISIBLE);
        }
        else
        {
            if(Who.pro == 1)
            {
                bottom.setVisibility(View.GONE);
                bottom2.setVisibility(View.VISIBLE);
            }
            attach.setVisibility(View.GONE);
        }

        return v;
    }


}
