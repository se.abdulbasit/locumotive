package com.example.project;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.project.Create_Account.hideKeyboard;
import static com.example.project.utils.Constants.USER_EMAIL;

public class CodeActivity extends AppCompatActivity {

    EditText ed1;
    EditText ed2;
    EditText ed3;
    EditText ed4;
    String email,password;
    String UserEmail = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);
        UserEmail = getIntent().getStringExtra(USER_EMAIL);
        RelativeLayout scroll = findViewById(R.id.rl);
        scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(CodeActivity.this);
            }
        });
        try{
            email=(String)getIntent().getExtras().get("email");
            password=(String)getIntent().getExtras().get("password");

        }catch (Exception c){ }
        ed1 = findViewById(R.id.ed1);
        ed2 = findViewById(R.id.ed2);
        ed3 = findViewById(R.id.ed3);
        ed4= findViewById(R.id.ed4);
        ed1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next ed_dob

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(ed1.getText().toString().trim().length()>=1){
                    ed2.requestFocus();
                }
                else
                {
                    ed1.requestFocus();
                }
            }
        });
        ed2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next ed_dob

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(ed2.getText().toString().trim().length()>=1){
                    ed3.requestFocus();
                }
                else
                {
                    ed1.requestFocus();
                }
            }
        });
        ed3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next ed_dob

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(ed3.getText().toString().trim().length()>=1){
                    ed4.requestFocus();
                }
                else
                {
                    ed2.requestFocus();
                }
            }
        });
        ed4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next ed_dob

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(ed4.getText().toString().trim().length()>=1){
                    ed4.requestFocus();
                }
                else
                {
                    ed3.requestFocus();
                }
            }
        });
        Button btn_signUp = findViewById(R.id.btn_signUp);
        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent ;
//                if(Who.select == 0)
//                    intent = new Intent(CodeActivity.this , create_account_next.class);
//                else
//                    intent = new Intent(CodeActivity.this , Create_Account.class);
//
//                startActivity(intent);
                profilevarifired();
            }
        });
    }
    public void profilevarifired(){
        try {
            String code=ed1.getText().toString()+ed2.getText().toString()+ed3.getText().toString()+ed4.getText().toString();
            Toast.makeText(getApplicationContext(),"called",Toast.LENGTH_LONG).show();
//            progressBar.show(this,"Loading..");
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = "http://134.209.29.174:3000/api/v1/verify-account";

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("verification_code", code);
            jsonBody.put( "email", UserEmail);



            JsonObjectRequest jobReq = new JsonObjectRequest(Request.Method.POST, URL, jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            try {
//                            Toast.makeText(getApplicationContext(),"response",Toast.LENGTH_LONG).show();
                                Log.d("Responses", jsonObject.toString());
                                int statuscod=jsonObject.getInt("status");
                                Log.i("Responses",statuscod+" this cod");
                                if(statuscod==200){

//                                MyUser.setId(jsonObject1.getInt("id"));

                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("email", email).apply();
                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("password", password).apply();
                                    Intent intent ;
                                    if(MyUser.getUser_type().equals("1"))
                                        intent = new Intent(CodeActivity.this , create_account_next.class);
                                    else
                                        intent = new Intent(CodeActivity.this , Create_Account.class);

                                    startActivity(intent);

//                               Toast.makeText(getApplicationContext(),"number added "+mobnumber,Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("responses", volleyError.toString());
//                            progressBar.getDialog().dismiss();
                            Toast.makeText(getApplicationContext(),"try again",Toast.LENGTH_LONG).show();
                        }
                    });

//            requestQueue.add(jobReq);
            Volley.newRequestQueue(this).add(jobReq);
        } catch (JSONException e) {
            e.printStackTrace();
//            progressBar.getDialog().dismiss();
            Toast.makeText(getApplicationContext(),"some thing went Wrong",Toast.LENGTH_LONG).show();
        }
    }

}
