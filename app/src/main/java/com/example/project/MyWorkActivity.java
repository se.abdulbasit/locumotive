package com.example.project;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import static com.example.project.DashboardActivity.setDrawer;

public class MyWorkActivity extends AppCompatActivity {

    TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_work);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        ImageView noti =  findViewById(R.id.noti);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ChatScreenActivity.this.finish();
                startActivity(new Intent(MyWorkActivity.this , NotificationActivity.class));
            }
        });
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        DashboardActivity.setMenuClick(this);
        setDrawer(this);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putInt("type", 0);
        Bundle bundle2 = new Bundle();
        bundle2.putInt("type", 1);
        Bundle bundle3 = new Bundle();
        bundle3.putInt("type", 2);
// set Fragmentclass Arguments
        workDetailFargment fragobj = new workDetailFargment();
        fragobj.setArguments(bundle);

        workDetailFargment fragobj2 = new workDetailFargment();
        fragobj2.setArguments(bundle2);

        workDetailFargment fragobj3 = new workDetailFargment();
        fragobj3.setArguments(bundle3);
        adapter.addFragment(fragobj, "Upcoming");
        adapter.addFragment(fragobj2, "Applied");
        adapter.addFragment(fragobj3, "Work History");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Log.e("error" , "  "+position);
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
