package com.example.project;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.Listner.GroupChildClick;
import com.example.project.model.IntrestedLocumModel;
import com.example.project.model.JobFeedModel;
import com.example.project.utils.CommonMethods;
import com.example.project.utils.Constants;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class jobAdapter extends BaseExpandableListAdapter {

    Context myContext;
    View view1;
    int ClickedGroup , ClickedChild = -1;
    GroupChildClick listner;
    ArrayList<JobFeedModel> list = new ArrayList<JobFeedModel>();

    public jobAdapter(Context myContext , ArrayList<JobFeedModel> list ,GroupChildClick listner )
    {
        this.myContext = myContext;
        this.list = list;
        this.listner = listner;


    }


    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return list.get(groupPosition).getLocums().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        view1 = LayoutInflater.from(myContext).inflate(R.layout.job_feed_item,parent,false);
        JobFeedModel m = list.get(groupPosition);
        TextView name = view1.findViewById(R.id.name);
        TextView price = view1.findViewById(R.id.price);
        TextView intrest = view1.findViewById(R.id.intrest);
        TextView cal = view1.findViewById(R.id.cal);
        TextView clock = view1.findViewById(R.id.clock);
        name.setText(m.getJobDetial().getJob_title());
        price.setText("£ "+m.getJobDetial().getMin_weekday_amount());
        intrest.setText("Intrested: "+m.getJobDetial().getIntrested());
        cal.setText(m.getJobDetial().getEnd_date());
        clock.setText(m.getJobDetial().getEnd_time());


        return view1;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        View v = LayoutInflater.from(myContext).inflate(R.layout.child_view,parent,false);;
        final IntrestedLocumModel model = list.get(groupPosition).getLocums().get(childPosition);
        ImageView correct = v.findViewById(R.id.correct);
        correct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickedGroup = groupPosition;
                ClickedChild = childPosition;
                RunApprovalApiCall(true , model.getUser_id() , list.get(groupPosition).getJobDetial().getJob_id());

            }
        });
        ImageView close = v.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickedGroup = groupPosition;
                ClickedChild = childPosition;
                RunApprovalApiCall(false , model.getUser_id() , list.get(groupPosition).getJobDetial().getJob_id());

            }
        });
        View avatar = v.findViewById(R.id.avatar);
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext , LocumProfileActivity.class);
                intent.putExtra("type" , 1);
                intent.putExtra("id" , list.get(groupPosition).getLocums().get(childPosition).getUser_id());
                myContext.startActivity(intent);
            }
        });
        LinearLayout avatar2 = v.findViewById(R.id.name);
        avatar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext , LocumProfileActivity.class);
                intent.putExtra("type" , 1);
                intent.putExtra("id" , list.get(groupPosition).getLocums().get(childPosition).getUser_id());
                myContext.startActivity(intent);
            }
        });

        TextView lname = v.findViewById(R.id.lname);
        lname.setText(model.getFirst_name()+" "+model.getLast_name());
        TextView lexp = v.findViewById(R.id.lexp);


        return v;
    }

    void RunApprovalApiCall(final boolean isApproved , final String userID , final String JobID)
    {
        Thread theThread =  new Thread(new Runnable() {
            @Override
            public void run() {
                listner.onSuccessRun(ClickedGroup , ClickedChild);
                CommonMethods.showProgressDialog((Activity) myContext);
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                    RequestBody body = RequestBody.create(mediaType, "job_id="+JobID+"&user_id="+userID+"&status="+(isApproved? "1" : "3"));
                    Request request = new Request.Builder()
                            .url("http://134.209.29.174:3000/api/v1/approved")
                            .post(body)
                            .addHeader("token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InV1bWFpcjYxOUBnbWFpbC5jb20iLCJpZCI6MTUsImlhdCI6MTU1NTU3NTM3MywiZXhwIjoxNTU1NjUwOTczfQ.eMrvoCcqP0i692b17gVfhLOwvsSVmfSoONC2TbmeiO0")
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("cache-control", "no-cache")
                            .addHeader("Postman-Token", "1538ab97-1fda-4812-9bf2-06aea4192df6")
                            .build();

                    Response response = client.newCall(request).execute();
                    CommonMethods.dismissProgressDialog();
                    if(response.code() == 200) {
                        String JsonResponse = response.body().string().toString();
                        JSONObject object = new JSONObject(JsonResponse);
                        Log.e("web" , " "+JsonResponse);
                        if (object.getInt("status") == 200)
                        {
                            ((Activity) myContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(myContext , (isApproved ? "Locum has been approved.":"Locum has been rejected."),Toast.LENGTH_SHORT).show();
                                }
                            });
                            if(isApproved)
                                list.remove(ClickedGroup);
                            else
                                list.get(ClickedGroup).getLocums().remove(ClickedChild);


                            ((Activity) myContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    notifyDataSetChanged();


                                }
                            });


                        }


                    }

                }
                catch (Exception e)
                {
                    Log.e("error" , "error" , e);

                }


            }
        });
        theThread.start();
    }
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}