package com.example.project;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.project.Listner.GroupChildClick;
import com.example.project.model.IntrestedLocumModel;
import com.example.project.model.JobFeedModel;
import com.example.project.model.JobModel;
import com.example.project.utils.AppProperties;
import com.example.project.utils.CommonMethods;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.project.DashboardActivity.setDrawer;
import static com.example.project.DashboardActivity.setMenuClick;

public class JobFeedActivity extends AppCompatActivity implements GroupChildClick {

    ExpandableListView expandableList;
    ArrayList<JobFeedModel> list = new ArrayList<JobFeedModel>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_feed);
        getData();

        ImageView add = findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JobFeedActivity.this , PostJobActivity.class));
            }
        });
        ImageView noti = findViewById(R.id.noti);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JobFeedActivity.this , NotificationActivity.class));
            }
        });
        setMenuClick(this);
        setDrawer(this);
    }
    private static final OkHttpClient client = new OkHttpClient().newBuilder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    public static void get(String url, Map<String,String> params, Callback responseCallback) {
        HttpUrl.Builder httpBuider = HttpUrl.parse(url).newBuilder();
        if (params != null) {
            for (Map.Entry<String, String> param : params.entrySet()) {
                httpBuider.addQueryParameter(param.getKey(), param.getValue());
            }
        }
    }
    void getData()
    {
        CommonMethods.showProgressDialog(this);
        list =  new ArrayList<JobFeedModel>();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {


                    OkHttpClient client = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url("http://134.209.29.174:3000/api/v1/store-jobs?store_id="+AppProperties.LoginUser.getUser_id())
                            .get()
                            .addHeader("token", AppProperties.LoginUser.getToken())
                            .addHeader("cache-control", "no-cache")
                            .addHeader("Postman-Token", "c6f90a02-ed6b-451b-8e5c-9ecf877ec974")
                            .build();

                    Response response = client.newCall(request).execute();
                    if(response.code() == 200) {
                        String JsonResponse = response.body().string().toString();
                        JSONObject object = new JSONObject(JsonResponse);
                        Log.e("web" , " "+JsonResponse);
                        if (object.getInt("status") == 200) {

                            JSONArray data = object.getJSONArray("data");
                            Gson gson =  new Gson();
                            for(int i = 0 ; i<data.length() ; i++)
                            {
                                JSONObject Object_Js = data.getJSONObject(i);
                                JobModel model = gson.fromJson(Object_Js.toString()  , JobModel.class);
                                ;
                                JobFeedModel m =  new JobFeedModel();
                                m.setJobDetial(model);
                                m.setLocums(getJobLocum(model.getJob_id()));
                                list.add(m);

                            }


                        }
                    }
                    }catch (Exception e)
                {
                    Log.e("error" , "error " , e);

                }
                CommonMethods.dismissProgressDialog();
                JobFeedActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setAdapter();
                    }
                });

            }
        });
        thread.start();
    }


    ArrayList<IntrestedLocumModel> getJobLocum(String jobID)
    {

        ArrayList<IntrestedLocumModel> model = new ArrayList<IntrestedLocumModel>();

        try
        {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url("http://134.209.29.174:3000/api/v1/applicants?job_id="+jobID)
                    .get()
                    .addHeader("cache-control", "no-cache")
                    .addHeader("Postman-Token", "836e4ea5-80f2-4930-a6a0-62c40849ea3b")
                    .build();

            Response response = client.newCall(request).execute();
            if(response.code() == 200)
            {
                String JsonResponse = response.body().string().toString();

                Log.e("web" , " "+JsonResponse);
                JSONObject object = new JSONObject(JsonResponse);
                if (object.getInt("status") == 200) {

                    JSONArray data = object.getJSONArray("data");
                    Gson gson =  new Gson();
                    for(int i = 0 ; i<data.length() ; i++)
                    {
                        JSONObject Object_Js = data.getJSONObject(i);
                        IntrestedLocumModel locumModel = gson.fromJson(Object_Js.toString()  , IntrestedLocumModel.class);
                        model.add(locumModel);

                    }


                }

            }
        }
        catch (Exception e)
        {

        }

        return model;

    }
    void setAdapter()
    {

        expandableList = ((ExpandableListView)findViewById(R.id.job));
        jobAdapter adapter = new jobAdapter(this , list , this);
        expandableList.setAdapter(adapter );
        expandableList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if(groupPosition != previousGroup)
                    expandableList.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });
    }

    public static void setMenuClick(final Activity activity)
    {
        Resources res = activity.getResources();
        final int newColor = res.getColor(R.color.colorPrimary);


        ImageView menu1 = activity.findViewById(R.id.menu1);
        ImageView menu2 = activity.findViewById(R.id.menu2);
        ImageView menu3 = activity.findViewById(R.id.menu3);
        ImageView menu4 = activity.findViewById(R.id.menu4);
        ImageView menu5 = activity.findViewById(R.id.menu5);
        menu1.setImageResource(R.drawable.menu1);
        menu2.setImageResource(R.drawable.menu2unselect);
        menu3.setImageResource(R.drawable.menu3);
        menu4.setImageResource(R.drawable.menu4);
        menu5.setImageResource(R.drawable.menu5);
        if((activity instanceof JobFeedActivity))
            menu1.setImageResource(R.drawable.menu1select);


        if((activity instanceof WorkActivity))
            menu2.setImageResource(R.drawable.menu2);

         if((activity instanceof InboxScreen))
            menu3.setImageResource(R.drawable.menu3select);


        if((activity instanceof ProfileActivity))
            menu4           .setImageResource(R.drawable.menu4select);


        if((activity instanceof SettingActivity))
            menu5           .setImageResource(R.drawable.menu5select);



        menu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {Who.pro = 0;
                if(!(activity instanceof JobFeedActivity))
                    activity.startActivity(new Intent(activity , JobFeedActivity.class));
            }
        });

        menu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {Who.pro = 0;
                if(!(activity instanceof WorkActivity))
                    activity.startActivity(new Intent(activity , WorkActivity.class));
            }
        });

        menu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {Who.pro = 0;
                if(!(activity instanceof InboxScreen))
                {
                    Intent intent  = new Intent(activity , InboxScreen.class);
                    intent.putExtra("c" , 1);
                    activity.startActivity(intent);
                }
            }
        });

        menu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {Who.pro = 0;
                if(!(activity instanceof ProfileActivity))
                    activity.startActivity(new Intent(activity , ProfileActivity.class));
            }
        });

        menu5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Who.pro = 0;
                if(!(activity instanceof SettingActivity))
                {
                    Intent intent  = new Intent(activity , SettingActivity.class);
                    intent.putExtra("c" , 1);
                    activity.startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onSuccessRun(final int GroupId, int ChildId) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                expandableList.collapseGroup(GroupId);
            }
        });
    }
}
