package com.example.project.utils;

import android.content.Context;

public class Constants {

    public static final String USER_KEY = "USER_KEY";
    public static final String STORE_KEY = "STORE_KEY";
    public static final String USER_EMAIL = "USER_EMAIL";

}
