package com.example.project.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

public class CommonMethods {
    static ProgressDialog dialog;
    static Activity myContext ;
    public static boolean isEmpty(String s)
    {
        if(s == null || TextUtils.isEmpty(s))
            return true;

        return false;
    }
    public static void showError(final Activity context , final String message)
    {
        try
        {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context , message , Toast.LENGTH_LONG).show();
                }
            });
        }
        catch (Exception e)
        {

        }



    }
    public static void showProgressDialog(final Activity context )
    {
        myContext = context;
        try
        {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog = new ProgressDialog(context);
                    dialog.show();
                }
            });
        }
        catch (Exception e)
        {

        }


    }
    public static void dismissProgressDialog()
    {
        try
        {
            myContext.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            });
        }
        catch (Exception e)
        {

        }

    }
}
