package com.example.project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Payment_Adapter extends BaseAdapter {

    Context myContext;
    View view1;

    public Payment_Adapter(Context myContext )
    {
        this.myContext = myContext;


    }
    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        view1 = LayoutInflater.from(myContext).inflate(R.layout.payment_item,parent,false);


        return view1;
    }
}