package com.example.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.project.model.StoreModel;
import com.example.project.model.UserModel;
import com.example.project.utils.AppProperties;
import com.example.project.utils.CommonMethods;
import com.google.gson.Gson;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.project.utils.CommonMethods.isEmpty;
import static com.example.project.utils.CommonMethods.showError;

public class ProfileActivity extends AppCompatActivity {
    TextView tvName,tvPrice,tvStartDate,tvTime,tvDetails,tvDistance,tvAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail_screen);
       /* View btn_next = findViewById(R.id.btn_next);
        btn_next.setVisibility(View.GONE);*/

       tvName=(TextView)findViewById(R.id.name);
       tvPrice=(TextView)findViewById(R.id.price);
       tvStartDate=(TextView)findViewById(R.id.start_date);
       tvTime=(TextView)findViewById(R.id.clock);
       tvDetails=(TextView)findViewById(R.id.description);
       tvDistance=(TextView)findViewById(R.id.distance);
       tvAddress=(TextView)findViewById(R.id.address);

        if (Who.select != 0)
            JobFeedActivity.setMenuClick(this);
        else
            DashboardActivity.setMenuClick(this);



        TextView title = findViewById(R.id.title);
        title.setText("Store Name");

        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileActivity.this.finish();
            }
        });

        if (Who.select == 0) {

            View bottom = findViewById(R.id.bottom);
            bottom.setVisibility(View.GONE);
        } else {
            ImageView create = findViewById(R.id.create);
            create.setVisibility(View.VISIBLE);
            create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ProfileActivity.this, Create_Account.class);
                    intent.putExtra("key", 1);
                    startActivity(intent);
                }
            });
            ImageView noti = findViewById(R.id.noti);
            noti.setVisibility(View.VISIBLE);
            noti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //ChatScreenActivity.this.finish();
                    startActivity(new Intent(ProfileActivity.this, NotificationActivity.class));
                }
            });
            View bottom = findViewById(R.id.bottom);
            bottom.setVisibility(View.VISIBLE);
            LinearLayout intrest;
            intrest = findViewById(R.id.intrest);
            intrest.setVisibility(View.GONE);
            LinearLayout info;
            info = findViewById(R.id.info);
            info.setVisibility(View.VISIBLE);


        }
        GetStoreDetails();
    }

    void GetStoreDetails() {


        CommonMethods.showProgressDialog(ProfileActivity.this);
        Thread networkThread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    OkHttpClient client = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url("http://134.209.29.174:3000/api/v1/store-profile/6"/* + AppProperties.LoginUser.getStore_id()*/)
                            .get()
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("token", AppProperties.LoginUser.getToken())
                            .addHeader("User-Agent", "PostmanRuntime/7.11.0")
                            .addHeader("Accept", "*/*")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Postman-Token", "e8519a55-20ef-41f8-ac74-f79084d1319b,89191bf0-4110-42d6-9493-f0ddb598d852")
                            .addHeader("Host", "134.209.29.174:3000")
                            .addHeader("accept-encoding", "gzip, deflate")
                            .addHeader("Connection", "keep-alive")
                            .addHeader("cache-control", "no-cache")
                            .build();

                    Response response = client.newCall(request).execute();


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            CommonMethods.dismissProgressDialog();
                        }
                    });
                    if (response.code() == 200) {
                        String JsonResponse = response.body().string();
                        JSONObject object = new JSONObject(JsonResponse);
                        Log.d("createAccount", "response : " + object.toString());

                        if (object.getInt("status") == 200) {
                            JSONObject data = object.getJSONObject("data");
                            Gson gson = new Gson();
                            final StoreModel model = gson.fromJson(data.toString(), StoreModel.class);
                            Log.d("ProfileActivity","Abdul : "+model.getDetail());
                          //  AppProperties.LoginStore = model;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    populateDate(model);
                                                              }
                            });



                        } else {
                            showError(ProfileActivity.this, "Status Code is not 200");
                        }
                    } else {
                        showError(ProfileActivity.this, "Some error occur please check your internet");
                    }
                } catch (Exception e) {

                }

            }
        });
        networkThread.start();


    }
    void populateDate(StoreModel model){
        tvName.setText(model.getStore_name());
        // tvPrice.setText(model.);,
        //tvStartDate.setText(model.st);
      //  tvTime.setText(model.getPrefered_testing_time());
        tvDetails.setText(model.getDetail());
        //tvDistance.setText(model.di);
        tvAddress.setText(model.getAddress());
    }
}
