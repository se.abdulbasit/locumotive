package com.example.project;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.project.model.UserModel;
import com.example.project.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity {

    EditText email,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        TextView signUp = findViewById(R.id.signUp);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent ;
                intent = new Intent(Login.this , SignUp.class);


                startActivity(intent);

            }
        });

        Button btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logincal();
//                Intent intent ;
//                if(Who.select == 0)
//                    startActivity(new Intent(Login.this , DashboardActivity.class));
//                else
//                    startActivity(new Intent(Login.this , JobFeedActivity.class));



            }
        });
    }
    public void logincal(){
        Toast.makeText(getApplicationContext(),"called",Toast.LENGTH_LONG).show();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

//            progressBar.show(this,"Loading..");
                   // RequestQueue requestQueue = Volley.newRequestQueue(this);
                    String URL = "http://134.209.29.174:3000/api/v1/login";

                    JSONObject jsonBody = new JSONObject();
                    jsonBody.put("email", email.getText().toString());
                    jsonBody.put( "password", password.getText().toString());

                    try {
                        OkHttpClient client = new OkHttpClient();

                        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                        RequestBody body = RequestBody.create(mediaType, "email="+ email.getText().toString().replaceAll("@","%40")+"&password="+password.getText().toString());
                        Request request = new Request.Builder()
                                .url("http://134.209.29.174:3000/api/v1/login")
                                .post(body)
                                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                                .addHeader("User-Agent", "PostmanRuntime/7.11.0")
                                .addHeader("Accept", "*/*")
                                .addHeader("Cache-Control", "no-cache")
                                .addHeader("Postman-Token", "32880be7-44ae-46f5-b7e0-8aae245cd351,3604129b-65d8-454e-be32-ecfe2a1137d8")
                                .addHeader("Host", "134.209.29.174:3000")
                                .addHeader("accept-encoding", "gzip, deflate")
                                .addHeader("content-length", "47")
                                .addHeader("Connection", "keep-alive")
                                .addHeader("cache-control", "no-cache")
                                .build();

                        Response response = client.newCall(request).execute();
                        JSONObject jsonObject = new JSONObject(response.body().string());
//                            Toast.makeText(getApplicationContext(),"response",Toast.LENGTH_LONG).show();
                        Log.d("Responses", jsonObject.toString());
                        int statuscod=jsonObject.getInt("status");
                        Log.i("Responses",statuscod+" this cod");
                        if(statuscod==200){

                            JSONObject jsonObject1=jsonObject.getJSONObject("data");
                            Log.d("Responses2", jsonObject1.toString());
                            MyUser.setUser_id(jsonObject1.getInt("user_id"));
                            MyUser.setFirst_name(jsonObject1.getString("first_name"));
                            MyUser.setPhone_no(jsonObject1.getString("phone_no"));
                            MyUser.setTc_box(jsonObject1.getString("tc_box"));
                            MyUser.setUser_type(jsonObject1.getString("user_type"));
                            MyUser.setToken(jsonObject1.getString("token"));
                            UserModel user = new UserModel();
                            user.setUser_id(MyUser.getUser_id());
                            user.setFirst_name(MyUser.getFirst_name());
                            user.setPhone_no(MyUser.getPhone_no());
                            user.setTc_box(MyUser.getTc_box());
                            user.setUser_type(MyUser.getUser_type());
                            user.setToken(MyUser.getToken());
                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("email", email.getText().toString()).apply();
                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("password", password.getText().toString()).apply();

                            Gson gson = new Gson();
                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(Constants.USER_KEY, gson.toJson(user)).apply();
                            if(jsonObject1.getString("user_type").equals("1")){
                                Who.select=0;
                            }else if(jsonObject1.getString("user_type").equals("0")){
                                Who.select=1;
                            }
                            Intent intent ;
                            if(Who.select == 0)
                                startActivity(new Intent(Login.this , DashboardActivity.class));
                            else
                                startActivity(new Intent(Login.this , JobFeedActivity.class));


//                                MyUser.setId(jsonObject1.getInt("id"));
//                                    String email=jsonObject1.getString("email");
////                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("email", email).apply();
////                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("password", passwprd.getText().toString()).apply();
//                                    Intent intent = new Intent(SignUp.this , CodeActivity.class);
//                                    intent.putExtra("email",email);
//                                    intent.putExtra("password",passwprd.getText().toString());
//                                    startActivity(intent);

//                               Toast.makeText(getApplicationContext(),"number added "+mobnumber,Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

/*
            JsonObjectRequest jobReq = new JsonObjectRequest(Request.Method.POST, URL, jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("responses", volleyError.toString());
//                            progressBar.getDialog().dismiss();
                            Toast.makeText(getApplicationContext(),"try again",Toast.LENGTH_LONG).show();
                        }
                    });

//            requestQueue.add(jobReq);
            Volley.newRequestQueue(this).add(jobReq);*/
                } catch (JSONException e) {
                    e.printStackTrace();
//            progressBar.getDialog().dismiss();
                    Toast.makeText(getApplicationContext(),"some thing went Wrong",Toast.LENGTH_LONG).show();
                }

            }
        });
        thread.start();
    }
}
