package com.example.project;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;


public class JobDetailScreen extends AppCompatActivity {

    LinearLayout intrest;
    IncomingJobsDetails data;
    private GoogleMap mMap;
    TextView name,price,startdate,timming,distance,jobdetails,address,jobid;
    ImageView parking,pretest,feildtest,photoropter,trailframe,contactlense;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail_screen);
        ImageView back = findViewById(R.id.back);
        name=findViewById(R.id.name);
        price=findViewById(R.id.price);
        startdate=findViewById(R.id.startdate);
        timming=findViewById(R.id.clock);
        distance=findViewById(R.id.distance);
        jobdetails=findViewById(R.id.jobdeatil);
        address=findViewById(R.id.address);
        jobid=findViewById(R.id.title);
        parking=findViewById(R.id.parking);
        feildtest=findViewById(R.id.feildtest);
        pretest=findViewById(R.id.pretest);
        photoropter=findViewById(R.id.phorotoper);
        trailframe=findViewById(R.id.trailframe);
        contactlense=findViewById(R.id.contactlense);
        try{
            data=(IncomingJobsDetails)getIntent().getExtras().get("data");
            name.setText(data.getJob_title());
            price.setText(data.getMix_weekend_amount()+"/day");
            startdate.setText(data.getStart_date());
            timming.setText(data.getStart_time()+" - "+data.getEnd_time());
            distance.setText(data.getDistance()+" miles away");
            jobdetails.setText(data.getJob_detail());
            address.setText(data.getCity()+" , "+data.getCountry()+" , "+data.getAddress());
            jobid.setText(data.getJob_id());
            if(data.getParking().equals("1")){
                parking.setImageResource(R.drawable.i11);
            }
            if(data.getPre_test().equals("1")){
                pretest.setImageResource(R.drawable.i21);
            }
            if(data.getField_test().equals("1")){
                feildtest.setImageResource(R.drawable.i31);
            }
            if(data.getPhorotoper().equals("1")){
                photoropter.setImageResource(R.drawable.i41);
            }
            if(data.getTrail_frame().equals("1")){
                trailframe.setImageResource(R.drawable.i51);
            }
        }catch (Exception c){
            Toast.makeText(getApplicationContext(),"Exception",Toast.LENGTH_LONG).show();
            c.printStackTrace();
            return;
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                // Add a marker in Sydney, Australia, and move the camera.

                try{
                    LatLng location=getCityLatitude(JobDetailScreen.this,data.getCity());
                    mMap.addMarker(new MarkerOptions().position(location).title(data.getCity()));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                    mMap.animateCamera( CameraUpdateFactory.zoomTo( 17.0f ) );
                }catch (Exception c){
                    LatLng location = new LatLng(-34, 151);
                    mMap.addMarker(new MarkerOptions().position(location).title("hard coded cordinates"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                    mMap.animateCamera( CameraUpdateFactory.zoomTo( 17.0f ) );
                }

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobDetailScreen.this.finish();
            }
        });


        intrest = findViewById(R.id.intrest);
        if(Who.select == 1)
            intrest.setVisibility(View.GONE);
    }


    public static LatLng getCityLatitude(Context context, String city) {
        Geocoder geocoder = new Geocoder(context,context.getResources().getConfiguration().locale);
        List<Address> addresses = null;
        LatLng latLng = null;
        try {
            addresses = geocoder.getFromLocationName(city, 1);
            Address address = addresses.get(0);
            latLng = new LatLng(address.getLatitude(), address.getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return latLng;
    }
}
