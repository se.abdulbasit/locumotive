package com.example.project;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.model.IntrestedLocumModel;
import com.example.project.model.JobFeedModel;
import com.example.project.model.JobModel;
import com.example.project.model.StoredBookedJobsModel;
import com.example.project.utils.CommonMethods;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ItemAdapter extends BaseAdapter implements View.OnClickListener {

    Context myContext;
    View view1;
    Boolean isWorkDetailFragmetn = false;
    int customerStart = 5, communicationStart = 5, punctualityStart = 5;
    int type;
    ArrayList<IncomingJobsDetails> items;
    ArrayList<StoredBookedJobsModel> storedBookedJobsModelArrayList;

    public ItemAdapter(Context myContext, int type, ArrayList<IncomingJobsDetails> it) {
        items = new ArrayList<>();
        items = it;
        this.myContext = myContext;
        this.type = type;


    }

    public ItemAdapter(Context myContext, int type, ArrayList<StoredBookedJobsModel> it, Boolean isWorkDetailFragmetn) {
        items = new ArrayList<>();
        storedBookedJobsModelArrayList = it;
        this.myContext = myContext;
        this.type = type;
        this.isWorkDetailFragmetn = isWorkDetailFragmetn;


    }

    @Override
    public int getCount() {
        if (isWorkDetailFragmetn == true) {
            return storedBookedJobsModelArrayList.size();
        }
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (type == 4) {
            view1 = LayoutInflater.from(myContext).inflate(R.layout.work_booked_item, null);
            TextView tvJobTittle, cal, clock, locamName, complete;
            tvJobTittle = view1.findViewById(R.id.tvJobTittle);
            cal = view1.findViewById(R.id.cal);
            clock = view1.findViewById(R.id.clock);
            locamName = view1.findViewById(R.id.locamName);
            complete = view1.findViewById(R.id.complete);

            StoredBookedJobsModel storedBookedJobsModel = storedBookedJobsModelArrayList.get(position);
            tvJobTittle.setText(storedBookedJobsModel.getJobTittle());
            cal.setText(storedBookedJobsModel.getStartDate());
            locamName.setText(storedBookedJobsModel.getFirstName() + " " + storedBookedJobsModel.getLastName());
            clock.setText(storedBookedJobsModel.getStartTime());
            complete.setText("Mark Complete");
        } else if (type == 10) {
            view1 = LayoutInflater.from(myContext).inflate(R.layout.item_view, null);
            TextView jobname, storename, startdata, time, price;
            jobname = view1.findViewById(R.id.name);
            storename = view1.findViewById(R.id.namep);
            startdata = view1.findViewById(R.id.startdate);
            time = view1.findViewById(R.id.time);
            price = view1.findViewById(R.id.price);

            StoredBookedJobsModel storedBookedJobsModel = storedBookedJobsModelArrayList.get(position);

            jobname.setText(storedBookedJobsModel.getJobTittle());
            storename.setText(storedBookedJobsModel.getCity());//This will be updated after words
            startdata.setText(storedBookedJobsModel.getStartDate());
            time.setText(storedBookedJobsModel.getStartTime() + " - " + storedBookedJobsModel.getEndTime());
            price.setText(storedBookedJobsModel.getMinWeekdayAmount() + " /day");

        } else {
            view1 = LayoutInflater.from(myContext).inflate(R.layout.item_view, null);
            TextView jobname, storename, startdata, time, price;
            jobname = view1.findViewById(R.id.name);
            storename = view1.findViewById(R.id.namep);
            startdata = view1.findViewById(R.id.startdate);
            time = view1.findViewById(R.id.time);
            price = view1.findViewById(R.id.price);

            jobname.setText(items.get(position).getJob_title());
            storename.setText(items.get(position).getCity());//This will be updated after words
            Log.d("***city", items.get(position).getCity());
            startdata.setText(items.get(position).getStart_date());
            time.setText(items.get(position).getStart_time() + " - " + items.get(position).getEnd_time());
            price.setText(items.get(position).getMin_weekday_amount() + " /day");


        }


        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isWorkDetailFragmetn == false) {
                    Intent i = new Intent(myContext, JobDetailScreen.class);
                    i.putExtra("data", items.get(position));
                    myContext.startActivity(i);
                }
            }
        });

        if ((TextView) view1.findViewById(R.id.namep) != null) {
            if (Who.select == 0) {
                ((TextView) view1.findViewById(R.id.namep)).setText("Store Name");
            } else {
                ((TextView) view1.findViewById(R.id.namep)).setText("Locum Name");
            }
        }
//        if((TextView)view1.findViewById(R.id.detail) != null)
//        {
//
//            ((TextView)view1.findViewById(R.id.detail)).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    myContext.startActivity(new Intent(myContext , JobDetailScreen.class));
//                }
//            });
//
//        }
        if ((TextView) view1.findViewById(R.id.re) != null && !isWorkDetailFragmetn) {
            final TextView voice = (TextView) view1.findViewById(R.id.re);
            if (items.get(position) == null) {
                voice.setText("re-send invoice");
            } else {
                voice.setText("Invoice send");
            }

//            ((TextView)view1.findViewById(R.id.re)).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(myContext);
//                    builder.setTitle("Invoice");
//                    builder.setMessage("Invoice has been resent");
//                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    });
//                    builder.show();
//                    items.set(position ,"sdds");
//                    voice.setText("Invoice send");
//                    voice.setOnClickListener(null);
//                }
//            });

        }
        if ((TextView) view1.findViewById(R.id.locamName) != null) {
            ((TextView) view1.findViewById(R.id.locamName)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(myContext, LocumProfileActivity.class);
                    intent.putExtra("type", 1);
                    myContext.startActivity(intent);
                }
            });
        }
        if ((TextView) view1.findViewById(R.id.complete) != null) {

            ((TextView) view1.findViewById(R.id.complete)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(myContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = inflater.inflate(R.layout.dialog_view, null, false);
                    final EditText descriptionEditText = view.findViewById(R.id.description);

                    final ImageView communicationStart1 = view.findViewById(R.id.communicationStart1);
                    final ImageView communicationStart2 = view.findViewById(R.id.communicationStart2);
                    final ImageView communicationStart3 = view.findViewById(R.id.communicationStart3);
                    final ImageView communicationStart4 = view.findViewById(R.id.communicationStart4);
                    final ImageView communicationStart5 = view.findViewById(R.id.communicationStart5);

                    final ImageView punctualityStart1 = view.findViewById(R.id.punctualityStart1);
                    final ImageView punctualityStart2 = view.findViewById(R.id.punctualityStart2);
                    final ImageView punctualityStart3 = view.findViewById(R.id.punctualityStart3);
                    final ImageView punctualityStart4 = view.findViewById(R.id.punctualityStart4);
                    final ImageView punctualityStart5 = view.findViewById(R.id.punctualityStart5);

                    final ImageView customerServieStart1 = view.findViewById(R.id.customerServieStart1);
                    final ImageView customerServieStart2 = view.findViewById(R.id.customerServieStart2);
                    final ImageView customerServieStart3 = view.findViewById(R.id.customerServieStart3);
                    final ImageView customerServieStart4 = view.findViewById(R.id.customerServieStart4);
                    final ImageView customerServieStart5 = view.findViewById(R.id.customerServieStart5);


                    communicationStart1.setOnClickListener(this);
                    communicationStart2.setOnClickListener(this);
                    communicationStart3.setOnClickListener(this);
                    communicationStart4.setOnClickListener(this);
                    communicationStart5.setOnClickListener(this);

                    punctualityStart1.setOnClickListener(this);
                    punctualityStart2.setOnClickListener(this);
                    punctualityStart3.setOnClickListener(this);
                    punctualityStart4.setOnClickListener(this);
                    punctualityStart5.setOnClickListener(this);

                    customerServieStart1.setOnClickListener(this);
                    customerServieStart2.setOnClickListener(this);
                    customerServieStart3.setOnClickListener(this);
                    customerServieStart4.setOnClickListener(this);
                    customerServieStart5.setOnClickListener(this);


                    ((View) view.findViewById(R.id.submit)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // here we have to write complete code

                            if (isWorkDetailFragmetn == true) {
                                addUserReview(position, descriptionEditText.getText().toString(), String.valueOf(communicationStart), String.valueOf(punctualityStart)
                                        , String.valueOf(customerStart));
                            }
                            dialog.dismiss();
                        }
                    });

                    //HERE YOU CAN FIND YOU IDS AND SET TEXTS OR BUTTONS
                    ((Activity) myContext).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    dialog.setContentView(view);
                    final Window window = dialog.getWindow();
                    window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    window.setBackgroundDrawableResource(android.R.color.transparent);
                    window.setGravity(Gravity.CENTER);
                    dialog.show();

                }
            });

        }
        if (type == 6) {
            try {
//                if((TextView)view1.findViewById(R.id.detail) != null)
//                {
//                    ((TextView)view1.findViewById(R.id.detail)).setVisibility(View.GONE);
//                    ((TextView)view1.findViewById(R.id.detail)).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            myContext.startActivity(new Intent(myContext , JobDetailScreen.class));
//                        }
//                    });
//
//                }


                if ((ImageView) view1.findViewById(R.id.star) != null)
                    ((ImageView) view1.findViewById(R.id.star)).setVisibility(View.VISIBLE);

            } catch (Exception e) {

            }
            try {
                final TextView notpaid = (TextView) view1.findViewById(R.id.notpaid);
                if (notpaid != null) {
                    notpaid.setVisibility(View.VISIBLE);
                    notpaid.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(myContext);
                            builder.setTitle("Confirmation");
                            builder.setMessage("Please Confirm the payment for the job has been sent to the locum?");
                            builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    notpaid.setText("Paid");
                                    notpaid.setOnClickListener(null);
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.show();
                        }
                    });
                }
            } catch (Exception e) {

            }


        }
        if (type == 2) {
            try {
//                if((TextView)view1.findViewById(R.id.detail) != null)
//                {
//                    ((TextView)view1.findViewById(R.id.detail)).setVisibility(View.GONE);
//                    ((TextView)view1.findViewById(R.id.detail)).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            myContext.startActivity(new Intent(myContext , JobDetailScreen.class));
//                        }
//                    });
//
//                }

                if ((TextView) view1.findViewById(R.id.re) != null)
                    ((TextView) view1.findViewById(R.id.re)).setVisibility(View.VISIBLE);

                if ((ImageView) view1.findViewById(R.id.star) != null)
                    ((ImageView) view1.findViewById(R.id.star)).setVisibility(View.VISIBLE);

            } catch (Exception e) {

            }


        }
        if (type == 5) {

//            try {
//            if ((TextView) view1.findViewById(R.id.detail) != null) {
//                    ((TextView) view1.findViewById(R.id.detail)).setVisibility(View.GONE);
//                    ((TextView) view1.findViewById(R.id.detail)).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            myContext.startActivity(new Intent(myContext, JobDetailScreen.class));
//                        }
//                    });
//                }
//            }catch (Exception e)
//            { }

        }

        return view1;
    }


    void addUserReview(final int position, final String reviewDescription, final String conversationStart, final String punctualityStart, final String customerServiceStars) {
        CommonMethods.showProgressDialog((Activity) myContext);
        final StoredBookedJobsModel storedBookedJobsModel = storedBookedJobsModelArrayList.get(position);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                RequestBody body = RequestBody.create(mediaType, "" +
                        "user_id=" + storedBookedJobsModel.getUserID()
                        + "&review_id=" + storedBookedJobsModel.getId()
                        + "&name=" + storedBookedJobsModel.getFirstName() + storedBookedJobsModel.getLastName()
                        + "&review_description=" + reviewDescription
                        + "&conversion_stars=" + conversationStart
                        + "&punctuality_stars=" + punctualityStart
                        + "&customer_service_stars=" + customerServiceStars
                        + "&job_id=" + storedBookedJobsModel.getJobID() + "");
                Request request = new Request.Builder()
                        .url("http://134.209.29.174:3000/api/v1/user-review")
                        .post(body)
                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .addHeader("token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImF5ZWVubXVoYW1tYWRAZ21haWwuY29tIiwiaWQiOjEyLCJpYXQiOjE1NTUyNTMxMjgsImV4cCI6MTU1NTMyODcyOH0.hlCWQ_AhTgPj3lx2WqkYTxIwDDPrUXAkGBXr31_SPsg")
                        .addHeader("cache-control", "no-cache")
                        .addHeader("Postman-Token", "638fc1e7-6ef3-4beb-9e9f-2d20154e215d")
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    if (response.code() == 200) {
                        Toast.makeText(myContext, "Review Submitted Successfully", Toast.LENGTH_LONG).show();
                        storedBookedJobsModelArrayList.remove(position);
                        notifyDataSetInvalidated();

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        thread.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.customerServieStart1:
                customerStart = 1;
                break;
            case R.id.customerServieStart2:
                customerStart = 2;
                break;
            case R.id.customerServieStart3:
                customerStart = 3;
                break;
            case R.id.customerServieStart4:
                customerStart = 4;
                break;
            case R.id.customerServieStart5:
                customerStart = 5;
                break;

            case R.id.punctualityStart1:
                punctualityStart = 1;
                break;
            case R.id.punctualityStart2:
                punctualityStart = 2;
                break;
            case R.id.punctualityStart3:
                punctualityStart = 3;
                break;
            case R.id.punctualityStart4:
                punctualityStart = 4;
                break;
            case R.id.punctualityStart5:
                punctualityStart = 5;
                break;

            case R.id.communicationStart1:
                communicationStart = 1;
                break;
            case R.id.communicationStart2:
                communicationStart = 2;
                break;
            case R.id.communicationStart3:
                communicationStart = 3;
                break;
            case R.id.communicationStart4:
                communicationStart = 4;
                break;
            case R.id.communicationStart5:
                communicationStart = 5;
                break;


        }
    }
}
