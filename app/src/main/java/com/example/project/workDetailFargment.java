package com.example.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.project.model.StoreModel;
import com.example.project.model.StoredBookedJobsModel;
import com.example.project.utils.AppProperties;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class workDetailFargment extends Fragment {

    ArrayList<IncomingJobsDetails> items;
    ArrayList<StoredBookedJobsModel> storedBookedJobsModelsList,storedBookedJobsModelsList2;
    int id, user_id;
    ItemAdapter adapter;

    public workDetailFargment() {
        // Required empty public constructor
    }


    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        items = new ArrayList<>();
        storedBookedJobsModelsList = new ArrayList<>();
        storedBookedJobsModelsList2 = new ArrayList<>();
        // Inflate the layout for this fragment
        int strtext = getArguments().getInt("type");
        v = inflater.inflate(R.layout.fragment_work_detail_fargment, container, false);
//"http://134.209.29.174:3000/api/v1/jobs?start_date=2019-02-02&end_date=2019-02-20&country=Pakistan&distance=3&testing_time=30 Mins&min_weekday_amount=100&min_weekend_amount=450&user_id=21"

        // to e comit
        //getjobsfeed("http://134.209.29.174:3000/api/v1/jobs?user_id=36", strtext);
        if (strtext == 5) {
            getStoredBookedJobs("http://134.209.29.174:3000/api/v1/store-booked-jobs/1", strtext);
        } else if (strtext == 6) {
            getCompleteJobs("sfdsf", strtext);
        }
//        String url="http://134.209.29.174:3000/api/v1/preferences?user_id="+MyUser.getUser_id();
//        getPrefrnce(url,strtext);
        return v;

    }

    public void getStoredBookedJobs(String URL, final int strtext) {
        Thread networkThread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url("http://134.209.29.174:3000/api/v1/store-booked-jobs/1")
                            .get()
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImF5ZWVubXVoYW1tYWRAZ21haWwuY29tIiwiaWQiOjEyLCJpYXQiOjE1NTU2OTA1OTUsImV4cCI6MTU1NTc2NjE5NX0.Pqzjqlt1-j143QOA1_2eSq7S5LjKwcqXX_PBEckeqyM")
                            .addHeader("cache-control", "no-cache")
                            .addHeader("Postman-Token", "69794b07-d243-4fd2-9be9-a3e4e69f36b1")
                            .build();

                    okhttp3.Response response = client.newCall(request).execute();
                    Gson gson = new Gson();
                    if (response.code() == 200) {
                        String JsonResponse = response.body().string();
                        JSONObject dataObject = new JSONObject(JsonResponse);
                        JSONArray jsonArray = dataObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            StoredBookedJobsModel contact = gson.fromJson(obj.toString(), StoredBookedJobsModel.class);
                            storedBookedJobsModelsList.add(contact);

                        }
                        Log.d("workDetalFragmet", "Stored Model list size :  " + storedBookedJobsModelsList.size());


                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                // Stuff that updates the UI
                                ItemAdapter adapter = new ItemAdapter(getActivity(), 4, storedBookedJobsModelsList,true);
                                ((ListView) v.findViewById(R.id.item)).setAdapter(adapter);


                            }
                        });

                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }


            }
        });
        networkThread.start();
    }

    public void getCompleteJobs(String URL, final int strtext) {
        Thread networkThread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    OkHttpClient client = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url("http://134.209.29.174:3000/api/v1/store-completed-jobs/1")
                            .get()
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImF5ZWVubXVoYW1tYWRAZ21haWwuY29tIiwiaWQiOjEyLCJpYXQiOjE1NTU3MDExOTgsImV4cCI6MTU1NTc3Njc5OH0.VFH7mV59XcGRljeT_1BMDceomcJkbWEjteuHMshA418")
                            .addHeader("cache-control", "no-cache")
                            .addHeader("Postman-Token", "445482f6-6276-42be-bca9-2db101b6fdfd")
                            .build();
                    okhttp3.Response response = client.newCall(request).execute();
                    Gson gson = new Gson();

                    if (response.code() == 200) {
                        String JsonResponse = response.body().string().toString();
                        JSONObject dataObject = new JSONObject(JsonResponse);
                        JSONArray jsonArray = dataObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            StoredBookedJobsModel contact = gson.fromJson(obj.toString(), StoredBookedJobsModel.class);
                            storedBookedJobsModelsList2.add(contact);

                        }
                        Log.d("workDetalFragmet", "Compelete Job Response :  " + storedBookedJobsModelsList2.size());


                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                // Stuff that updates the UI
                                ItemAdapter adapter2 = new ItemAdapter(getActivity(), 10, storedBookedJobsModelsList2,true );
                                ((ListView) v.findViewById(R.id.item)).setAdapter(adapter2);


                            }
                        });

                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }


            }
        });
        networkThread.start();
    }

    public void getjobsfeed(String URL, final int strtext) {
     /*   try {
//            progressBar.show(this,"Loading..");
//            Toast.makeText(getApplicationContext(),"called",Toast.LENGTH_LONG).show();
            RequestQueue requestQueue = Volley.newRequestQueue(DashboardActivity.activity);
//            String URL = "http://134.209.29.69:5001/api/User/who-like-me?user_id="+id;
//            String URL="http://68.183.37.116:5001/api/User/who-like-me?user_id="+id;
//                String URLDELTE="http://68.183.37.116:5001/api/User/deleteMyAccount";
//                JSONObject jsonBodytodelte = new JSONObject();
//                jsonBodytodelte.put("user_id", "Tariq zia");


            JsonObjectRequest jobReq = new JsonObjectRequest(Request.Method.GET, URL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
//                            Toast.makeText(getApplicationContext(),"response",Toast.LENGTH_LONG).show();
                            Log.d("Responses", jsonObject.toString());
                            try {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                Log.i("***arrylenth", jsonArray.length() + "");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    Log.i("***objs", obj.toString());
                                    settingDataintoArraylist(obj);
                                }
//                                init();
//                                progressBar.getDialog().dismiss();
                                ItemAdapter adapter = new ItemAdapter(getActivity(), strtext, items);
                                ((ListView) v.findViewById(R.id.item)).setAdapter(adapter);

                            } catch (Exception d) {
//                                progressBar.getDialog().dismiss();
                                Toast.makeText(DashboardActivity.activity, "some thing went wrong\ntry again", Toast.LENGTH_LONG).show();
                                d.printStackTrace();
//                                finish();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("responses", volleyError.toString());
//                            progressBar.getDialog().dismiss();

                        }
                    });


            requestQueue.add(jobReq);
        } catch (Exception e) {
            e.printStackTrace();
//            progressBar.getDialog().dismiss();

        }*/
    }

    public void settingDataintoArraylist(JSONObject jsonObject1) {
        try {
            IncomingJobsDetails data = new IncomingJobsDetails(
                    jsonObject1.getInt("id"),
                    jsonObject1.getInt("store_id"),
                    jsonObject1.getString("job_title"),
                    jsonObject1.getString("job_id"),
                    jsonObject1.getString("country"),
                    jsonObject1.getString("city"),
                    jsonObject1.getString("distance"),
                    jsonObject1.getString("start_date"),
                    jsonObject1.getString("end_date"),
                    jsonObject1.getString("perday_amount"),
                    jsonObject1.getString("start_time"),
                    jsonObject1.getString("end_time"),
                    jsonObject1.getString("min_weekday_amount"),
                    jsonObject1.getString("min_weekend_amount"),
                    jsonObject1.getString("testing_time"),
                    jsonObject1.getString("job_detail"),
                    jsonObject1.getString("pre_test"),
                    jsonObject1.getString("field_test"),
                    jsonObject1.getString("phorotoper"),
                    jsonObject1.getString("trail_frame"),
                    jsonObject1.getString("intrested"),
                    jsonObject1.getInt("payment_status"),
                    jsonObject1.getString("skills"),
                    jsonObject1.getString("address"),
                    jsonObject1.getString("parking"));

//            Log.i("alldata***",data.getAlldata());
            items.add(data);
            Log.i("***size", items.size() + "");
        } catch (Exception d) {
            d.printStackTrace();
            Log.e("***Errer", d.getMessage() + " cause " + d.getCause());
        }
    }

}
