package com.example.project;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {
    TabLayout tabLayout;
    private static ViewPager viewPager;
    public static int flag = 0;
    static Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        activity=DashboardActivity.this;
        setupViewPager(viewPager);
        ImageView bell = findViewById(R.id.bell);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this , NotificationActivity.class));
            }
        });
        ImageView add = findViewById(R.id.add);
        Glide.with(this)
                .load(R.drawable.n_p)
                .apply(RequestOptions.circleCropTransform())
                .into(add);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        flag = 1 ;
        ImageView filter = findViewById(R.id.filter);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this , FilterActivity.class));
            }
        });

        setMenuClick(this);
        setDrawer(this);

    }

    public static void chageTab()
    {
        viewPager.setCurrentItem(0);

    }

    public static void setDrawer(final Activity activity)
    {
        ImageView draw = activity.findViewById(R.id.draw);
        final DrawerLayout drawerLayout = activity.findViewById(R.id.drawerLayout);
        draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(drawerLayout.isDrawerOpen(Gravity.START))
                    drawerLayout.closeDrawer(Gravity.START);
                else
                    drawerLayout.openDrawer(Gravity.START);
            }
        });
        NavigationView navigationView = activity.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id)
                {
                    case R.id.dash:

                        drawerLayout.closeDrawer(Gravity.START);
                        if(activity instanceof DashboardActivity || activity instanceof JobFeedActivity)
                            break;
                        Intent intent1 ;
                        if(Who.select == 0)
                            intent1 = new Intent(activity , DashboardActivity.class);
                        else
                            intent1 = new Intent(activity , JobFeedActivity.class);

                        activity.startActivity(intent1);


                        /*activity.startActivity(new Intent(activity , DashboardActivity.class));*/
                        break;

                    case R.id.offers:
                        drawerLayout.closeDrawer(Gravity.START);
                        activity.startActivity(new Intent(activity , Order_Cancel.class));
                        break;

                    case R.id.inbox:
                        drawerLayout.closeDrawer(Gravity.START);
                        Intent intent = new Intent(activity , InboxScreen.class);
                        intent.putExtra("c" , Who.select);
                        activity.startActivity(intent);
                        break;
                    case R.id.Notification:
                        drawerLayout.closeDrawer(Gravity.START);
                        activity.startActivity(new Intent(activity , NotificationActivity.class));
                        break;
                    case R.id.pay:
                        drawerLayout.closeDrawer(Gravity.START);
                        activity.startActivity(new Intent(activity , PaymentHistory.class));
                        break;
                        case R.id.Contact:
                        case R.id.feed:
                        case R.id.report:
                        drawerLayout.closeDrawer(Gravity.START);
                        activity.startActivity(new Intent(activity , ContactActivity.class));
                        break;
                        case R.id.term:
                        drawerLayout.closeDrawer(Gravity.START);
                        activity.startActivity(new Intent(activity , TermAndConditions.class));
                        break;
                        case R.id.pp:
                        drawerLayout.closeDrawer(Gravity.START);
                        activity.startActivity(new Intent(activity , Policy.class));
                        break;
                        case R.id.logout:
                        drawerLayout.closeDrawer(Gravity.START);
                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
                            builder.setMessage("Are you sure you want to logout?")
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            Intent intent = new Intent(activity , Who.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                                            activity.startActivity(intent);
                                        }
                                    });
                            builder.show();
                        break;
                }

                return false;
            }
        });
    }
    public static void setMenuClick(final Activity activity)
    {

        ImageView menu1 = activity.findViewById(R.id.menu1);
        ImageView menu2 = activity.findViewById(R.id.menu2);
        ImageView menu3 = activity.findViewById(R.id.menu3);
        ImageView menu4 = activity.findViewById(R.id.menu4);
        ImageView menu5 = activity.findViewById(R.id.menu5);
        menu1.setImageResource(R.drawable.menu1);
        menu2.setImageResource(R.drawable.menu2unselect);
        menu3.setImageResource(R.drawable.menu3);
        menu4.setImageResource(R.drawable.menu4);
        menu5.setImageResource(R.drawable.menu5);
        if((activity instanceof DashboardActivity))
            menu1.setImageResource(R.drawable.menu1select);


        if((activity instanceof MyWorkActivity))
            menu2.setImageResource(R.drawable.menu2);

        if((activity instanceof InboxScreen))
            menu3.setImageResource(R.drawable.menu3select);


        if((activity instanceof LocumProfileActivity))
            menu4           .setImageResource(R.drawable.menu4select);


        if((activity instanceof SettingActivity))
            menu5           .setImageResource(R.drawable.menu5select);





        menu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(activity instanceof DashboardActivity))
                    activity.startActivity(new Intent(activity , DashboardActivity.class));
            }
        });
        menu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(activity instanceof MyWorkActivity))
                    activity.startActivity(new Intent(activity , MyWorkActivity.class));
            }
        });
        menu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(activity instanceof InboxScreen))
                    activity.startActivity(new Intent(activity , InboxScreen.class));
            }
        });
        menu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(activity instanceof LocumProfileActivity))
                {
                    activity.startActivity(new Intent(activity , LocumProfileActivity.class));
                }
            }
        });
        menu5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(activity instanceof SettingActivity))
                    activity.startActivity(new Intent(activity , SettingActivity.class));
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        DashboardActivity.ViewPagerAdapter adapter = new DashboardActivity.ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putInt("type", 0);
        Bundle bundle2 = new Bundle();
        bundle2.putInt("type", 0);
        Bundle bundle3 = new Bundle();
        bundle3.putInt("type", 2);
// set Fragmentclass Arguments
        workDetailFargment fragobj = new workDetailFargment();
        fragobj.setArguments(bundle);

        prefrence fragobj2 = new prefrence();
        fragobj2.setArguments(bundle2);

        workDetailFargment fragobj3 = new workDetailFargment();
        fragobj3.setArguments(bundle3);
        adapter.addFragment(fragobj, "News Feed");
        adapter.addFragment(fragobj2, "Preferences");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Log.e("error" , "  "+position);
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
