package com.example.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Who extends AppCompatActivity {


    public static int select = -1 ;
    public static int pro = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_who);
        final Button store = findViewById(R.id.store);
        final Button locum = findViewById(R.id.locum);
        final Button btn_login = findViewById(R.id.btn_login);
        store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                locum.setBackgroundResource(R.drawable.show_password_selector);
                store.setBackgroundResource(R.drawable.bg);
                select = 1 ;
               // btn_login.performClick();
            }
        });
        locum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select = 0;
                locum.setBackgroundResource(R.drawable.bg);
                store.setBackgroundResource(R.drawable.show_password_selector);
             //   btn_login.performClick();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(select == -1)
                    return;
                startActivity(new Intent(Who.this , SignUp.class));

            }
        });

    }
}
