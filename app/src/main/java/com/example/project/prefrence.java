package com.example.project;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.example.project.Create_Account.hideKeyboard;
import static com.example.project.DashboardActivity.chageTab;


///**
// * A simple {@link Fragment} subclass.
// * Activities that contain this fragment must implement the
// * {@link prefrence.OnFragmentInteractionListener} interface
// * to handle interaction events.
// * Use the {@link prefrence#newInstance} factory method to
// * create an instance of this fragment.
// */
public class prefrence extends Fragment {

    Spinner country,distance,time;
    EditText minweekday,maxweekday, enddate, startdate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vlc;
        vlc = inflater.inflate(R.layout.fragment_prefrence, container, false);

        LinearLayout scroll = vlc.findViewById(R.id.rl);
        scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(getActivity());
            }
        });
        country=vlc.findViewById(R.id.job);
        distance=vlc.findViewById(R.id.job2);
        time=vlc.findViewById(R.id.job3);
        minweekday=vlc.findViewById(R.id.minweekday);
        maxweekday=vlc.findViewById(R.id.maxweekday);
        final Calendar myCalendar = Calendar.getInstance();

         startdate = (EditText) vlc.findViewById(R.id.bob);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                startdate.setText(sdf.format(myCalendar.getTime()));
            }

        };

        startdate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



          enddate = (EditText) vlc.findViewById(R.id.dob);
        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                enddate.setText(sdf.format(myCalendar.getTime()));
            }

        };

        enddate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        Button btn_next3 = vlc.findViewById(R.id.btn_next3);
        btn_next3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPrefrence();
//                chageTab();
            }
        });

        return   vlc;
    }


    public void setPrefrence(){
        try {
            Toast.makeText(DashboardActivity.activity,"called",Toast.LENGTH_LONG).show();
//            progressBar.show(this,"Loading..");
//            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = "http://134.209.29.174:3000/api/v1/preferences";

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("user_id", MyUser.getUser_id());
            jsonBody.put( "start_date", startdate.getText().toString());
            jsonBody.put( "end_date", enddate.getText().toString());
            jsonBody.put( "country", String.valueOf(country.getSelectedItem()));
            jsonBody.put( "distance", String.valueOf(distance.getSelectedItem()));
            jsonBody.put( "testing_time", String.valueOf(time.getSelectedItem()));
            jsonBody.put( "min_weekday_amount", minweekday.getText().toString());
            jsonBody.put( "min_weekend_amount", minweekday.getText().toString());
//            jsonBody.put( "equipment_preferred", String.valueOf(equipment_prefer.getSelectedItem()));
//

            JsonObjectRequest jobReq = new JsonObjectRequest(Request.Method.POST, URL, jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            try {
//                            Toast.makeText(getApplicationContext(),"response",Toast.LENGTH_LONG).show();
                                Log.d("Responses", jsonObject.toString());
                                int statuscod=jsonObject.getInt("status");
                                Log.i("Responses",statuscod+" this cod");
                                if(statuscod==200){

//                                    JSONObject jsonObject1=jsonObject.getJSONObject("data");
//                                    Log.d("Responses2", jsonObject1.toString());
                                    chageTab();
////                                MyUser.setId(jsoationContext(),"number added "+mobnumber,Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("responses", volleyError.toString());
//                            progressBar.getDialog().dismiss();
                            Toast.makeText(DashboardActivity.activity,"try again",Toast.LENGTH_LONG).show();
                        }
                    });

//            requestQueue.add(jobReq);
            Volley.newRequestQueue(DashboardActivity.activity).add(jobReq);
        } catch (JSONException e) {
            e.printStackTrace();
//            progressBar.getDialog().dismiss();
            Toast.makeText(DashboardActivity.activity,"some thing went Wrong",Toast.LENGTH_LONG).show();
        }
    }

}
