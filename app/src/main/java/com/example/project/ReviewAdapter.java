package com.example.project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class ReviewAdapter extends BaseAdapter {

    View view1;
    Context myContext ;
    public ReviewAdapter(Context context)
    {
        myContext = context;

    }
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        view1 = LayoutInflater.from(myContext).inflate(R.layout.review_item,parent,false);
        return view1;
    }
}
