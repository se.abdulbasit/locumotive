package com.example.project;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


public class ReviewFragment extends Fragment {


    public ReviewFragment() {
        // Required empty public constructor
    }

    View v;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_review, container, false);
        ListView list = v.findViewById(R.id.list);
        ReviewAdapter adapter = new ReviewAdapter(getActivity());
        list.setAdapter(adapter);
        return v;
    }


}
