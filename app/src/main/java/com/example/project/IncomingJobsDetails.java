package com.example.project;

import java.io.Serializable;

public class IncomingJobsDetails implements Serializable {
        int  id,store_id;
        String job_title;
        String job_id;
        String country;
        String city,distance,start_date,end_date,perday_amount,start_time,end_time,min_weekday_amount;
        String mix_weekend_amount,testing_time;
         String job_detail,pre_test,field_test,phorotoper,trail_frame,intrested;
         int payment_status;String skills,address,parking;

    public IncomingJobsDetails(int id, int store_id, String job_title,
                               String job_id, String country, String city,
                               String distance, String start_date, String end_date,
                               String perday_amount, String start_time, String end_time,
                               String min_weekday_amount, String mix_weekend_amount,
                               String testing_time, String job_detail, String pre_test,
                               String field_test, String phorotoper, String trail_frame,
                               String intrested,int payment_status,String skills,
                               String address,String parking) {
        this.id = id;
        this.store_id = store_id;
        this.job_title = job_title;
        this.job_id = job_id;
        this.country = country;
        this.city = city;
        this.distance = distance;
        this.start_date = start_date;
        this.end_date = end_date;
        this.perday_amount = perday_amount;
        this.start_time = start_time;
        this.end_time = end_time;
        this.min_weekday_amount = min_weekday_amount;
        this.mix_weekend_amount = mix_weekend_amount;
        this.testing_time = testing_time;
        this.job_detail = job_detail;
        this.pre_test = pre_test;
        this.field_test = field_test;
        this.phorotoper = phorotoper;
        this.trail_frame = trail_frame;
        this.intrested = intrested;
        this.payment_status=payment_status;
        this.skills=skills;
        this.address=address;
        this.parking=parking;
    }

    public int getId() {
        return id;
    }

    public int getStore_id() {
        return store_id;
    }

    public String getJob_title() {
        return job_title;
    }

    public String getJob_id() {
        return job_id;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getDistance() {
        return distance;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public String getPerday_amount() {
        return perday_amount;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public String getMin_weekday_amount() {
        return min_weekday_amount;
    }

    public String getMix_weekend_amount() {
        return mix_weekend_amount;
    }

    public String getTesting_time() {
        return testing_time;
    }

    public String getJob_detail() {
        return job_detail;
    }

    public String getPre_test() {
        return pre_test;
    }

    public String getField_test() {
        return field_test;
    }

    public String getPhorotoper() {
        return phorotoper;
    }

    public String getTrail_frame() {
        return trail_frame;
    }

    public String getIntrested() {
        return intrested;
    }

    public int getPayment_status() {
        return payment_status;
    }

    public String getSkills() {
        return skills;
    }

    public String getAddress() {
        return address;
    }

    public String getParking() {
        return parking;
    }
}
