package com.example.project;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.example.project.DashboardActivity.setDrawer;

public class LocumProfileActivity extends AppCompatActivity {

    ImageView star;
    ImageView edit2;
    ImageView attach;
    LinearLayout skill;
    public static int type = 0;
    int id = -1;
   // RelativeLayout bottom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locum_profile);
        TextView title = findViewById(R.id.title);
        ImageView noti =  findViewById(R.id.noti);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ChatScreenActivity.this.finish();
                startActivity(new Intent(LocumProfileActivity.this , NotificationActivity.class));
            }
        });
        if(Who.select == 0)
            title.setText("My Profile");
        else
            title.setText("Locum Profile");
        star = (ImageView) findViewById(R.id.star);
        edit2 = (ImageView) findViewById(R.id.edit2);
        edit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LocumProfileActivity.this , create_account_next.class);
                intent.putExtra("key" , 1);
                startActivity(intent);
            }
        });
        attach = (ImageView) findViewById(R.id.attach);
        attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),101);
            }
        });
        //bottom =  findViewById(R.id.bottom);
        skill =  findViewById(R.id.skill);
        int type = getIntent().getIntExtra("type" , 0);
        id = getIntent().getIntExtra("id" , -1);
        this.type = type;
        DashboardActivity.setMenuClick(this);
        if(type == 1)
        {
            edit2.setVisibility(View.GONE);
            View v = findViewById(R.id.bottom22);
            v.setVisibility(View.GONE);
            ImageView draw = findViewById(R.id.draw);
            draw.setImageResource(R.drawable.ic_keyboard_arrow_left_black_24dp);
            draw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocumProfileActivity.this.finish();
                }
            });
        }
        if(true)
        {

            star.setVisibility(View.VISIBLE);
            RelativeLayout tabsRTl = findViewById(R.id.tabsRTl);
            tabsRTl.setVisibility(View.VISIBLE);
            //bottom.setVisibility(View.VISIBLE);
            //skill.setVisibility(View.VISIBLE);


            setView();


        }
        else
        {
            setDrawer(this);
        }

    }

    void setView()
    {
        TabLayout tabLayout;
        ViewPager viewPager;

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }
    private void setupViewPager(ViewPager viewPager) {
        LocumProfileActivity.ViewPagerAdapter adapter = new LocumProfileActivity.ViewPagerAdapter(getSupportFragmentManager());


// set Fragmentclass Arguments
        profileview fragobj = new profileview();
        ReviewFragment fragobj2 = new ReviewFragment();
        adapter.addFragment(fragobj, "Profile");
        adapter.addFragment(fragobj2, "Review");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Log.e("error" , "  "+position);
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
