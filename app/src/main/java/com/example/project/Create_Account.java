package com.example.project;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.project.model.StoreModel;
import com.example.project.model.UserModel;
import com.example.project.utils.AppProperties;
import com.example.project.utils.CommonMethods;
import com.google.gson.Gson;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.project.utils.CommonMethods.isEmpty;
import static com.example.project.utils.CommonMethods.showError;

public class Create_Account extends AppCompatActivity {

    boolean b1 = false;
    boolean b2 = false;
    boolean b3 = false;
    boolean b4 = false;
    boolean b5 = false;
    boolean b6 = false;
    EditText store_name , postalCode , address2 ,weekEnd  , weekDay , des;
    Spinner equipment;
    Spinner time;
    RadioButton y;
    String s_Name , s_PostalCode , s_Address , s_WeekEnd , s_WeekDay , s_Des;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create__account);
        store_name = findViewById(R.id.store_name);
        postalCode = findViewById(R.id.address);
        address2 = findViewById(R.id.address2);
        weekEnd = findViewById(R.id.weekEnd);
        weekDay = findViewById(R.id.weekDay);
        equipment = findViewById(R.id.equipment);
        time = findViewById(R.id.time);
        y = findViewById(R.id.y);
        des = findViewById(R.id.des);

        final int type = getIntent().getIntExtra("key" , 0 );

        RelativeLayout rtl = findViewById(R.id.rtl);
        rtl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(Create_Account.this);
            }
        });
        Button btn_next  = findViewById(R.id.btn_next);
        final ImageView i1 = findViewById(R.id.i1);
        LinearLayout linear = findViewById(R.id.linear);
        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectimage();
            }
        });
        LinearLayout l1 = findViewById(R.id.l1);
        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b1)
                    i1.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i1.setColorFilter(getResources().getColor(R.color.blue));
                b1 = !b1;
            }
        });

        final ImageView i2 = findViewById(R.id.i2);
        LinearLayout l2 = findViewById(R.id.l2);
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b2)
                    i2.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i2.setColorFilter(getResources().getColor(R.color.blue));
                b2 = !b2;
            }
        });

        final ImageView i3 = findViewById(R.id.i3);
        LinearLayout l3 = findViewById(R.id.l3);
        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b3)
                    i3.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i3.setColorFilter(getResources().getColor(R.color.blue));
                b3 = !b3;
            }
        });

        final ImageView i4 = findViewById(R.id.i4);
        LinearLayout l4 = findViewById(R.id.l4);
        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b4)
                    i4.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i4.setColorFilter(getResources().getColor(R.color.blue));
                b4 = !b4;
            }
        });

        final ImageView i5 = findViewById(R.id.i5);
        LinearLayout l5 = findViewById(R.id.l5);
        l5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b5)
                    i5.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i5.setColorFilter(getResources().getColor(R.color.blue));
                b5 = !b5;
            }
        });
        final ImageView i6 = findViewById(R.id.i6);
        LinearLayout l6 = findViewById(R.id.l6);
        l6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b6)
                    i6.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i6.setColorFilter(getResources().getColor(R.color.blue));
                b6 = !b6;
            }
        });
        i1.setColorFilter(getResources().getColor(R.color.light_gray3));
        i2.setColorFilter(getResources().getColor(R.color.light_gray3));
        i3.setColorFilter(getResources().getColor(R.color.light_gray3));
        i4.setColorFilter(getResources().getColor(R.color.light_gray3));
        i5.setColorFilter(getResources().getColor(R.color.light_gray3));
        i6.setColorFilter(getResources().getColor(R.color.light_gray3));



        if(type == 1)
        {
            btn_next.setText("Save");
            TextView create_account = findViewById(R.id.create_account);
            create_account.setText("Edit Profile");
        }
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(type == 1)
                    Create_Account.this.finish();
                else if(type==3){
                    AddStore();
                }
                else
                {
                    CreateMyAccount();
                    }
            }
        });
    }

    /**********************************************************************************************************/

    void CreateMyAccount ()
    {
//        startActivity(new Intent(Create_Account.this , JobFeedActivity.class));
        s_Name = store_name.getText().toString();
        s_PostalCode = postalCode.getText().toString();
        s_Address = address2.getText().toString();
        s_WeekEnd = weekEnd.getText().toString();
        s_WeekDay = weekDay.getText().toString();
        s_Des = des.getText().toString();
        if(isEmpty(s_Name))
        {
            store_name.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_PostalCode))
        {
            postalCode.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_Address))
        {
            address2.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_WeekEnd))
        {
            weekEnd.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_WeekDay))
        {
            weekDay.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_Des))
        {
            des.setError("This field cannot be empty.");
            return;
        }

        Thread networkThread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                    RequestBody body = RequestBody.create(mediaType, "store_name="+s_Name+"&address="+
                            address2+"&weekend_rate="+s_WeekEnd+"&equipment="+equipment.getSelectedItem().toString()+
                            "&parking_available="+(b1?"1":"0")+"&pre_test_required="+(b2?"1":"0")+"&detail="+
                            des+"&preferred_testing_time="+time.getSelectedItem().toString()+"&latitude=53.25655558&longitude=73.25655888&undefined=");
                    Request request = new Request.Builder()
                            .url("http://134.209.29.174:3000/api/v1/store-profile")
                            .post(body)
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("cache-control", "no-cache")
                            .addHeader("token", AppProperties.RegisterationToken)
                            .build();

                    Response response = client.newCall(request).execute();
                    if(response.code() == 200)
                    {
                        String JsonResponse = response.body().string();
                        JSONObject object = new JSONObject(JsonResponse);
                        if(object.getInt("status") == 200)
                        {
                            JSONObject data = object.getJSONObject("data");
                            Gson gson =  new Gson();
                            StoreModel model = gson.fromJson(data.toString() , StoreModel.class);
                            AppProperties.LoginStore = model;
                            UserModel model1 =  new UserModel();
                            model1.setToken(AppProperties.RegisterationToken);
                            model1.setUser_type("1");
                            model1.setTc_box("true");
                            model1.setPhone_no("");
                            model1.setFirst_name(model.getStore_name());
                            model1.setUser_id(Integer.valueOf(model.getId()));
                            model1.setEmail("");
                            AppProperties.LoginUser = model1;
                            startActivity(new Intent(Create_Account.this , JobFeedActivity.class));

                        }
                        else
                        {
                            showError(Create_Account.this , "Status Code is not 200");
                        }
                    }
                    else
                    {
                        showError(Create_Account.this , "Some error occur please check your internet");
                    }
                }
                catch (Exception e)
                {

                }

            }
        });
        networkThread.start();



    }


    void AddStore ()
    {
//        startActivity(new Intent(Create_Account.this , JobFeedActivity.class));
        s_Name = store_name.getText().toString();
        s_PostalCode = postalCode.getText().toString();
        s_Address = address2.getText().toString();
        s_WeekEnd = weekEnd.getText().toString();
        s_WeekDay = weekDay.getText().toString();
        s_Des = des.getText().toString();
        if(isEmpty(s_Name))
        {
            store_name.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_PostalCode))
        {
            postalCode.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_Address))
        {
            address2.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_WeekEnd))
        {
            weekEnd.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_WeekDay))
        {
            weekDay.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_Des))
        {
            des.setError("This field cannot be empty.");
            return;
        }


        CommonMethods.showProgressDialog(Create_Account.this);
        Thread networkThread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                 /*   OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                    RequestBody body = RequestBody.create(mediaType, "user_id="+AppProperties.LoginUser.getUser_id()
                            +"&store_name="+s_Name
                            +"&address="+s_Address
                            +"&weekend_rate="+weekDay
                            +"&equipment="+equipment.getSelectedItem()
                            +"&parking_available="+(b1?"1":"0")+
                                    "&pre_test_required="+(b2?"1":"0")+"&detail="+
                            des+"&preferred_testing_time="+time.getSelectedItem().toString()+"&latitude=53.25655558&longitude=73.25655888&undefined="
                            +"&pre_test_required=1&detail=dhakjfhkaf&preferred_testing_time=4%3A00PM&latitude=53.25655558&longitude=73.25655888&weekday_rate="+s_WeekDay
                            +"&field_test=1&phoropter=1&trail_frame=1&contact_lens=");
                    Request request = new Request.Builder()
                            .url("http://134.209.29.174:3000/api/v1/new-account")
                            .post(body)
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("token", AppProperties.LoginUser.getToken())
                            .addHeader("cache-control", "no-cache")
                            .addHeader("Postman-Token", "85157cf3-fad7-49bf-ac19-3bb2ae4a3679")
                            .build();

*/
                    OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                    RequestBody body = RequestBody.create(mediaType, "user_id="+AppProperties.LoginUser.getUser_id()
                            +"&store_name="+s_Name
                            +"&address="+s_Address
                            +"&weekend_rate="+s_WeekEnd
                            +"&equipment="+equipment.getSelectedItem()
                            +"&parking_available="+(b1?"1":"0")
                            +"pre_test_required=1"
                            +"&detail="+s_Des
                            +"&preferred_testing_time=4%3A00PM&latitude=53.25655558&longitude=73.25655888"
                            +"&weekday_rate="+s_WeekDay
                            +"&field_test=1&phoropter=1&trail_frame=1&contact_lens=");
                    Request request = new Request.Builder()
                            .url("http://134.209.29.174:3000/api/v1/new-account")
                            .post(body)
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("token", AppProperties.LoginUser.getToken())
                            .addHeader("User-Agent", "PostmanRuntime/7.11.0")
                            .addHeader("Accept", "*/*")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Postman-Token", "c324109e-bbcf-46d0-8455-0dd53dd270a9,3f310b0b-8c56-413b-893d-ad476b357c95")
                            .addHeader("Host", "134.209.29.174:3000")
                            .addHeader("accept-encoding", "gzip, deflate")
                            .addHeader("content-length", "293")
                            .addHeader("Connection", "keep-alive")
                            .addHeader("cache-control", "no-cache")
                            .build();

                  //  Response response = client.newCall(request).execute();


                    Response response = client.newCall(request).execute();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            CommonMethods.dismissProgressDialog();
                        }
                    });
                    if(response.code() == 200)
                    {
                        String JsonResponse = response.body().string().toString();
                        JSONObject object = new JSONObject(JsonResponse);
                        Log.d("createAccount","response : "+object.toString());

                        if(object.getInt("status") == 200)
                        {
                            JSONObject data = object.getJSONObject("data");
                            Gson gson =  new Gson();
                            StoreModel model = gson.fromJson(data.toString() , StoreModel.class);
                            AppProperties.LoginStore = model;
                            UserModel model1 =  new UserModel();
                          /*  model1.setToken(AppProperties.RegisterationToken);
                            model1.setUser_type("1");
                            model1.setTc_box("true");
                            model1.setPhone_no("");
                            model1.setFirst_name(model.getStore_name());
                            model1.setUser_id(Integer.valueOf(model.getId()));
                            model1.setEmail("");*/
                          //  AppProperties.LoginUser = model1;
                            startActivity(new Intent(Create_Account.this , JobFeedActivity.class));

                        }
                        else
                        {
                            showError(Create_Account.this , "Status Code is not 200");
                        }
                    }
                    else
                    {
                        showError(Create_Account.this , "Some error occur please check your internet");
                    }
                }
                catch (Exception e)
                {

                }

            }
        });
        networkThread.start();



    }


    void selectimage()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),101);
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
