package com.example.project.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Created by Abdul-Basit on 19/04/2019.
 */
public class StoredBookedJobsModel {
    @SerializedName("id")
    @Expose
    int id;
    @SerializedName("user_id")
    @Expose
    int userID;
    @SerializedName("job_id")
    @Expose
    String jobID;
    @SerializedName("status")
    @Expose
    int status;
    @SerializedName("applied_date")
    @Expose
    String appliedDate;
    @SerializedName("description")
    @Expose
    String description;
    @SerializedName("payment_status")
    @Expose
    int paymentStatus;
    @SerializedName("cancellation_date")
    @Expose
    String cancellationDate;
    @SerializedName("store_id")
    @Expose
    int storeID;
    @SerializedName("contact_lens")
    @Expose
    int contactLens;
    @SerializedName("job_title")
    @Expose
    String jobTittle;
    @SerializedName("country")
    @Expose
    String country;
    @SerializedName("city")
    @Expose
    String city;
    @SerializedName("distance")
    @Expose
    String distance;
    @SerializedName("start_date")
    @Expose
    String startDate;
    @SerializedName("end_date")
    @Expose
    String endDate;
    @SerializedName("perday_amount")
    @Expose
    String perDayAmount;
    @SerializedName("start_time")
    @Expose
    String startTime;
    @SerializedName("end_time")
    @Expose
    String endTime;
    @SerializedName("min_weekday_amount")
    @Expose
    String minWeekdayAmount;
    @SerializedName("min_weekend_amount")
    @Expose
    String minWeekEndAmount;
    @SerializedName("testing_time")
    @Expose
    String testingTime;
    @SerializedName("job_detail")
    @Expose
    String jobDetail;
    @SerializedName("no_posts")
    @Expose
    String noPosts;
    @SerializedName("skills")
    @Expose
    String skills;
    @SerializedName("address")
    @Expose
    String address;
    @SerializedName("parking")
    @Expose
    String parking;
    @SerializedName("pre_test")
    @Expose
    String preTest;
    @SerializedName("field_test")
    @Expose
    String fieldText;
    @SerializedName("phorotoper")
    @Expose
    String phorotoper;
    @SerializedName("trail_frame")
    @Expose
    String trail;
    @SerializedName("intrested")
    @Expose
    String intrested;
    @SerializedName("first_name")
    @Expose
    String firstName;

    public void setId(int id) {
        this.id = id;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setAppliedDate(String appliedDate) {
        this.appliedDate = appliedDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPaymentStatus(int paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public void setCancellationDate(String cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public void setContactLens(int contactLens) {
        this.contactLens = contactLens;
    }

    public void setJobTittle(String jobTittle) {
        this.jobTittle = jobTittle;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPerDayAmount() {
        return perDayAmount;
    }

    public void setPerDayAmount(String perDayAmount) {
        this.perDayAmount = perDayAmount;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getMinWeekdayAmount() {
        return minWeekdayAmount;
    }

    public void setMinWeekdayAmount(String minWeekdayAmount) {
        this.minWeekdayAmount = minWeekdayAmount;
    }

    public String getMinWeekEndAmount() {
        return minWeekEndAmount;
    }

    public void setMinWeekEndAmount(String minWeekEndAmount) {
        this.minWeekEndAmount = minWeekEndAmount;
    }

    public String getTestingTime() {
        return testingTime;
    }

    public void setTestingTime(String testingTime) {
        this.testingTime = testingTime;
    }

    public String getJobDetail() {
        return jobDetail;
    }

    public void setJobDetail(String jobDetail) {
        this.jobDetail = jobDetail;
    }

    public String getNoPosts() {
        return noPosts;
    }

    public void setNoPosts(String noPosts) {
        this.noPosts = noPosts;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public String getPreTest() {
        return preTest;
    }

    public void setPreTest(String preTest) {
        this.preTest = preTest;
    }

    public String getFieldText() {
        return fieldText;
    }

    public void setFieldText(String fieldText) {
        this.fieldText = fieldText;
    }

    public String getPhorotoper() {
        return phorotoper;
    }

    public void setPhorotoper(String phorotoper) {
        this.phorotoper = phorotoper;
    }

    public String getTrail() {
        return trail;
    }

    public void setTrail(String trail) {
        this.trail = trail;
    }

    public String getIntrested() {
        return intrested;
    }

    public void setIntrested(String intrested) {
        this.intrested = intrested;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGocNumber() {
        return gocNumber;
    }

    public void setGocNumber(String gocNumber) {
        this.gocNumber = gocNumber;
    }

    public String getOplNumber() {
        return oplNumber;
    }

    public void setOplNumber(String oplNumber) {
        this.oplNumber = oplNumber;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getInsuranceNo() {
        return insuranceNo;
    }

    public void setInsuranceNo(String insuranceNo) {
        this.insuranceNo = insuranceNo;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getEquipmentPreferred() {
        return equipmentPreferred;
    }

    public void setEquipmentPreferred(String equipmentPreferred) {
        this.equipmentPreferred = equipmentPreferred;
    }

    public String getYearOfExperience() {
        return yearOfExperience;
    }

    public void setYearOfExperience(String yearOfExperience) {
        this.yearOfExperience = yearOfExperience;
    }

    public String getPreferedTestingTime() {
        return preferedTestingTime;
    }

    public void setPreferedTestingTime(String preferedTestingTime) {
        this.preferedTestingTime = preferedTestingTime;
    }

    public String getOplProof() {
        return oplProof;
    }

    public void setOplProof(String oplProof) {
        this.oplProof = oplProof;
    }

    public String getGocProof() {
        return gocProof;
    }

    public void setGocProof(String gocProof) {
        this.gocProof = gocProof;
    }

    public String getInsuranceProof() {
        return insuranceProof;
    }

    public void setInsuranceProof(String insuranceProof) {
        this.insuranceProof = insuranceProof;
    }

    @SerializedName("last_name")
    @Expose
    String lastName;
    @SerializedName("date_of_birth")
    @Expose
    String DOB;
    @SerializedName("goc_number")
    @Expose
    String gocNumber;
    @SerializedName("opl_number")
    @Expose
    String oplNumber;
    @SerializedName("insurance_company")
    @Expose
    String insuranceCompany;
    @SerializedName("insurance_no")
    @Expose
    String insuranceNo;
    @SerializedName("profile_photo")
    @Expose
    String profilePhoto;
    @SerializedName("equipment_preferred")
    @Expose
    String equipmentPreferred;
    @SerializedName("year_of_experience")
    @Expose
    String yearOfExperience;
    @SerializedName("preferred_testing_time")
    @Expose
    String preferedTestingTime;
    @SerializedName("opl_proof")
    @Expose
    String oplProof;
    @SerializedName("goc_proof")
    @Expose
    String gocProof;
    @SerializedName("insurance_proof")
    @Expose
    String insuranceProof;

    public String getDescription() {
        return description;
    }

    public String getJobTittle() {
        return jobTittle;
    }

    public String getCancellationDate() {
        return cancellationDate;
    }

    public String getAppliedDate() {
        return appliedDate;
    }

    public int getId() {
        return id;
    }

    public int getPaymentStatus() {
        return paymentStatus;
    }

    public int getContactLens() {
        return contactLens;
    }

    public int getStatus() {
        return status;
    }

    public int getStoreID() {
        return storeID;
    }
}
