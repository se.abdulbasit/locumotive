package com.example.project.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Created by Abdul-Basit on 18/04/2019.
 */
public class BookedJobsModel {
    @SerializedName("id")
    @Expose
    String id;
    @SerializedName("user_id")
    @Expose
    String userId;
    @SerializedName("job_id")
    @Expose
    String jobId;
    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("applied_date")
    @Expose
    String appliedDate;
    @SerializedName("description")
    @Expose
    String description;
    @SerializedName("payment_status")
    @Expose
    String paymentStatus;
    @SerializedName("cancellation_date")
    @Expose
    String cancellationDate;
    @SerializedName("store_id")
    @Expose
    String storeId;
    @SerializedName("job_title")
    @Expose
    String jobTittle;
    @SerializedName("perday_amount")
    @Expose
    String perDayAmount;
    @SerializedName("first_name")
    @Expose
    String firstName;
    @SerializedName("last_name")
    @Expose
    String lastName;

    public String getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }

    public String getJobId() {
        return jobId;
    }

    public String getAppliedDate() {
        return appliedDate;
    }

    public String getDescription() {
        return description;
    }

    public String getUserId() {
        return userId;
    }

    public String getCancellationDate() {
        return cancellationDate;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getJobTittle() {
        return jobTittle;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPerDayAmount() {
        return perDayAmount;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAppliedDate(String appliedDate) {
        this.appliedDate = appliedDate;
    }

    public void setCancellationDate(String cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setJobTittle(String jobTittle) {
        this.jobTittle = jobTittle;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPerDayAmount(String perDayAmount) {
        this.perDayAmount = perDayAmount;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
}
