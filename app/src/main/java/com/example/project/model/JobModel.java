package com.example.project.model;

import android.util.Log;

public class JobModel {
    String id;
    String start_date;
    String end_date;
    String start_time;
    String end_time;
    String min_weekday_amount;

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getMin_weekday_amount() {
        return min_weekday_amount;
    }

    public void setMin_weekday_amount(String min_weekday_amount) {
        this.min_weekday_amount = min_weekday_amount;
    }

    public String getMin_weekend_amount() {
        return min_weekend_amount;
    }

    public void setMin_weekend_amount(String min_weekend_amount) {
        this.min_weekend_amount = min_weekend_amount;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getIntrested() {
        return intrested;
    }

    public void setIntrested(String intrested) {
        this.intrested = intrested;
    }

    String min_weekend_amount;
    String payment_status;
    String skills;
    String intrested;
    String job_title;
    String country;
    String distance;
    String testing_time;
    String store_id;
    String job_id;
    String no_posts;
    String perday_amount;
    String job_detail;
    String address;
    String parking;
    String pre_test;
    String field_test;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTesting_time() {
        return testing_time;
    }

    public void setTesting_time(String testing_time) {
        this.testing_time = testing_time;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getNo_posts() {
        return no_posts;
    }

    public void setNo_posts(String no_posts) {
        this.no_posts = no_posts;
    }

    public String getPerday_amount() {
        return perday_amount;
    }

    public void setPerday_amount(String perday_amount) {
        this.perday_amount = perday_amount;
    }

    public String getJob_detail() {
        return job_detail;
    }

    public void setJob_detail(String job_detail) {
        this.job_detail = job_detail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public String getPre_test() {
        return pre_test;
    }

    public void setPre_test(String pre_test) {
        this.pre_test = pre_test;
    }

    public String getField_test() {
        return field_test;
    }

    public void setField_test(String field_test) {
        this.field_test = field_test;
    }

    public String getPhorotoper() {
        return phorotoper;
    }

    public void setPhorotoper(String phorotoper) {
        this.phorotoper = phorotoper;
    }

    public String getTrail_frame() {
        return trail_frame;
    }

    public void setTrail_frame(String trail_frame) {
        this.trail_frame = trail_frame;
    }

    String phorotoper;
    String trail_frame;
    public void  print()
    {
        Log.e("Error " , job_detail + " "+ intrested);
    }
}
