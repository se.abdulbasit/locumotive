package com.example.project.model;

public class UserModel {

    // this class is reponseable for all the basic info a user can have in our app
    //we have different user (locum / store / etc ) so this will only have the basic user info
    int user_id;
    String first_name;
    String last_name;
    String phone_no;
    String tc_box;
    String User_type;
    String token;
    String email;
    String store_id;

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public  String getLast_name() {
        return last_name;
    }

    public  void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public  String getEmail() {
        return email;
    }

    public  void setEmail(String email) {
        this.email = email;
    }

    public  int getUser_id() {
        return user_id;
    }

    public  void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public  String getFirst_name() {
        return first_name;
    }

    public  void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public  String getPhone_no() {
        return phone_no;
    }

    public  void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public  String getTc_box() {
        return tc_box;
    }

    public  void setTc_box(String tc_box) {
        this.tc_box = tc_box;
    }

    public  String getUser_type() {
        return User_type;
    }

    public  void setUser_type(String user_type) {
        User_type = user_type;
    }

    public  String getToken() {
        return token;
    }

    public  void setToken(String token) {
        this.token = token;
    }
}