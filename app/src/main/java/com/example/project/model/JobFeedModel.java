package com.example.project.model;

import com.example.project.LocumProfileActivity;

import java.util.ArrayList;

public class JobFeedModel {
    public JobModel getJobDetial() {
        return jobDetial;
    }

    public void setJobDetial(JobModel jobDetial) {
        this.jobDetial = jobDetial;
    }

    public ArrayList<IntrestedLocumModel> getLocums() {
        return locums;
    }

    public void setLocums(ArrayList<IntrestedLocumModel> locums) {
        this.locums = locums;
    }

    JobModel jobDetial;
    ArrayList<IntrestedLocumModel> locums ;
}
