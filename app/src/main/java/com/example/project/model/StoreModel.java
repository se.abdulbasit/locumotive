package com.example.project.model;

public class StoreModel {
    String id;
    String store_name;
    String address;
    String weekend_rate;
    String equipment;
    String parking_available;
    String pre_test_required;
    String detail;
    String latitude;
    String longitude;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWeekend_rate() {
        return weekend_rate;
    }

    public void setWeekend_rate(String weekend_rate) {
        this.weekend_rate = weekend_rate;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getParking_available() {
        return parking_available;
    }

    public void setParking_available(String parking_available) {
        this.parking_available = parking_available;
    }

    public String getPre_test_required() {
        return pre_test_required;
    }

    public void setPre_test_required(String pre_test_required) {
        this.pre_test_required = pre_test_required;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPrefered_testing_time() {
        return prefered_testing_time;
    }

    public void setPrefered_testing_time(String prefered_testing_time) {
        this.prefered_testing_time = prefered_testing_time;
    }

    String prefered_testing_time;

}
