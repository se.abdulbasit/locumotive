package com.example.project;

public class MyUser {
   static int user_id;
   static String first_name;
    static String last_name;
    static String phone_no;
   static String tc_box;
   static String User_type;
   static String token;
    static String email;

    public static String getLast_name() {
        return last_name;
    }

    public static void setLast_name(String last_name) {
        MyUser.last_name = last_name;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        MyUser.email = email;
    }

    public static int getUser_id() {
        return user_id;
    }

    public static void setUser_id(int user_id) {
        MyUser.user_id = user_id;
    }

    public static String getFirst_name() {
        return first_name;
    }

    public static void setFirst_name(String first_name) {
        MyUser.first_name = first_name;
    }

    public static String getPhone_no() {
        return phone_no;
    }

    public static void setPhone_no(String phone_no) {
        MyUser.phone_no = phone_no;
    }

    public static String getTc_box() {
        return tc_box;
    }

    public static void setTc_box(String tc_box) {
        MyUser.tc_box = tc_box;
    }

    public static String getUser_type() {
        return User_type;
    }

    public static void setUser_type(String user_type) {
        User_type = user_type;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        MyUser.token = token;
    }
}
