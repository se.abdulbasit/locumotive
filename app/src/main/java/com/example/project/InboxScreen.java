package com.example.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import static com.example.project.DashboardActivity.setDrawer;
import static com.example.project.DashboardActivity.setMenuClick;

public class InboxScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_screen);
        ImageView noti =  findViewById(R.id.noti);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ChatScreenActivity.this.finish();
                startActivity(new Intent(InboxScreen.this , NotificationActivity.class));
            }
        });
        if(Who.select == 1)
            Who.pro = 1;
        int c = getIntent().getIntExtra("c" , 0);
        if(c == 1)
            JobFeedActivity.setMenuClick(this);
        else
            DashboardActivity.setMenuClick(this);

        RelativeLayout r1 = findViewById(R.id.r1);
        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(InboxScreen.this , ChatScreenActivity.class));
            }
        });

        RelativeLayout r5 = findViewById(R.id.r5);
        r5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(InboxScreen.this , ChatScreenActivity.class));
            }
        });

        RelativeLayout r2 = findViewById(R.id.r2);
        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(InboxScreen.this , ChatScreenActivity.class));
            }
        });

        RelativeLayout r3 = findViewById(R.id.r3);
        r3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(InboxScreen.this , ChatScreenActivity.class));
            }
        });

        RelativeLayout r4 = findViewById(R.id.r4);
        r4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(InboxScreen.this , ChatScreenActivity.class));
            }
        });

        if(c == 1)
        {
            View avatar = findViewById(R.id.avatar);
            avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , LocumProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });


            View avatar3 = findViewById(R.id.avatar3);
            avatar3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , LocumProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });


            View avatar44 = findViewById(R.id.avatar44);
            avatar44.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , LocumProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });


            View avatar4 = findViewById(R.id.avatar4);
            avatar4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , LocumProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });


            View avatar5 = findViewById(R.id.avatar5);
            avatar5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , LocumProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });

        }
        else
        {
            View avatar = findViewById(R.id.avatar);
            avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , ProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });


            View avatar3 = findViewById(R.id.avatar3);
            avatar3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , ProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });


            View avatar44 = findViewById(R.id.avatar44);
            avatar44.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , ProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });


            View avatar4 = findViewById(R.id.avatar4);
            avatar4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , ProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });


            View avatar5 = findViewById(R.id.avatar5);
            avatar5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(InboxScreen.this , ProfileActivity.class);
                    intent.putExtra("type" , 1);
                    startActivity(intent);
                }
            });
        }

        setDrawer(this);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Who.pro = 0;
    }
}
