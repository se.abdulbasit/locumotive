package com.example.project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class FilterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        RelativeLayout apply = findViewById(R.id.apply);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterActivity.this.finish();
            }
        });


        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterActivity.this.finish();
            }
        });
        final CheckBox american = findViewById(R.id.american) ;
        final CheckBox mEast = findViewById(R.id.mEast) ;
        final CheckBox chines = findViewById(R.id.chines) ;
        final CheckBox healty = findViewById(R.id.healty) ;


        healty.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mEast.setChecked(false);
                    chines.setChecked(false);
                    american.setChecked(false);
                }
            }
        });
        american.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mEast.setChecked(false);
                    chines.setChecked(false);
                    healty.setChecked(false);
                }
            }
        });
        mEast.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    american.setChecked(false);
                    chines.setChecked(false);
                    healty.setChecked(false);
                }
            }
        });
        chines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mEast.setChecked(false);
                    american.setChecked(false);
                    healty.setChecked(false);
                }
            }
        });
    }
}
