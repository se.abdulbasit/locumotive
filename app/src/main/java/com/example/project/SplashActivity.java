package com.example.project;

import android.app.Application;
import android.content.Intent;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.project.model.UserModel;
import com.example.project.utils.AppProperties;
import com.example.project.utils.Constants;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashActivity extends AppCompatActivity {
    ImageView iv;
    String email,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        email= PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("email", "");
        password=PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("password", "");


        iv=findViewById(R.id.splashimage);
        iv.setAlpha(0f);
        iv.animate().alpha(1f).setDuration(3000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String user_j = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(Constants.USER_KEY, null);
                if(user_j == null && (email.equals("") || password.equals("")))
                {
                    Intent i=new Intent(SplashActivity.this,Who.class);
                    startActivity(i);
                }
                else
                {
                    Gson gson = new Gson();
                    AppProperties.LoginUser = gson.fromJson(user_j , UserModel.class);
                    if(AppProperties.LoginUser.getUser_type().equals("1"))
                        startActivity(new Intent(SplashActivity.this , DashboardActivity.class));
                    else
                        startActivity(new Intent(SplashActivity.this , JobFeedActivity.class));

                }

                if(email.equals("") || password.equals("")){
                        Intent i=new Intent(SplashActivity.this,Who.class);
                        startActivity(i);
                }else{
                    logincal();
                }

                }
        } , 4000);

    }

    public void logincal(){
        try {
            Toast.makeText(getApplicationContext(),"called",Toast.LENGTH_LONG).show();
//            progressBar.show(this,"Loading..");
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = "http://134.209.29.174:3000/api/v1/login";

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("email", email);
            jsonBody.put( "password", password);




            JsonObjectRequest jobReq = new JsonObjectRequest(Request.Method.POST, URL, jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            try {
//                            Toast.makeText(getApplicationContext(),"response",Toast.LENGTH_LONG).show();
                                Log.d("Responses", jsonObject.toString());
                                int statuscod=jsonObject.getInt("status");
                                Log.i("Responses",statuscod+" this cod");
                                if(statuscod==200){

                                    JSONObject jsonObject1=jsonObject.getJSONObject("data");

                                    Log.d("Responses2", jsonObject1.toString());
                                    MyUser.setUser_id(jsonObject1.getInt("user_id"));
                                    MyUser.setFirst_name(jsonObject1.getString("first_name"));
                                    MyUser.setPhone_no(jsonObject1.getString("phone_no"));
                                    MyUser.setTc_box(jsonObject1.getString("tc_box"));
                                    MyUser.setUser_type(jsonObject1.getString("user_type"));
                                    MyUser.setToken(jsonObject1.getString("token"));
                                    UserModel user = new UserModel();
                                    user.setUser_id(MyUser.getUser_id());
                                    user.setFirst_name(MyUser.getFirst_name());
                                    user.setPhone_no(MyUser.getPhone_no());
                                    user.setTc_box(MyUser.getTc_box());
                                    user.setUser_type(MyUser.getUser_type());
                                    user.setToken(MyUser.getToken());
                                    Gson gson = new Gson();
                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(Constants.USER_KEY, gson.toJson(user)).apply();

                                    if(jsonObject1.getString("user_type").equals("1")){
                                        Who.select=0;
                                    }else if(jsonObject1.getString("user_type").equals("0")){
                                        Who.select=1;
                                    }

                                    Intent intent ;
                                    if(MyUser.getUser_type().equals("1"))
                                        startActivity(new Intent(SplashActivity.this , DashboardActivity.class));
                                    else
                                        startActivity(new Intent(SplashActivity.this , JobFeedActivity.class));


//                                MyUser.setId(jsonObject1.getInt("id"));
//                                    String email=jsonObject1.getString("email");
////                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("email", email).apply();
////                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("password", passwprd.getText().toString()).apply();
//                                    Intent intent = new Intent(SignUp.this , CodeActivity.class);
//                                    intent.putExtra("email",email);
//                                    intent.putExtra("password",passwprd.getText().toString());
//                                    startActivity(intent);

//                               Toast.makeText(getApplicationContext(),"number added "+mobnumber,Toast.LENGTH_LONG).show();
                                }
                                else if(statuscod==404){
                                    Intent i=new Intent(SplashActivity.this,Who.class);
                                    startActivity(i);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("responses", volleyError.toString());
//                            progressBar.getDialog().dismiss();
                            Toast.makeText(getApplicationContext(),"try again",Toast.LENGTH_LONG).show();
                        }
                    });

//            requestQueue.add(jobReq);
            Volley.newRequestQueue(this).add(jobReq);
        } catch (JSONException e) {
            e.printStackTrace();
//            progressBar.getDialog().dismiss();
            Toast.makeText(getApplicationContext(),"some thing went Wrong",Toast.LENGTH_LONG).show();
        }
    }
}
