package com.example.project;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import static com.example.project.DashboardActivity.setDrawer;

public class WorkActivity extends AppCompatActivity {
    TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        JobFeedActivity.setMenuClick(this);
        ImageView add = findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WorkActivity.this , PostJobActivity.class));
            }
        });
        setDrawer(this);
    }

    private void setupViewPager(ViewPager viewPager) {
        WorkActivity.ViewPagerAdapter adapter = new WorkActivity.ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putInt("type", 4);
        Bundle bundle2 = new Bundle();
        bundle2.putInt("type", 5);
        Bundle bundle3 = new Bundle();
        bundle3.putInt("type", 6);
// set Fragmentclass Arguments
        workDetailFargment fragobj = new workDetailFargment();
        fragobj.setArguments(bundle);

        workDetailFargment fragobj2 = new workDetailFargment();
        fragobj2.setArguments(bundle2);
        ImageView noti =  findViewById(R.id.noti);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ChatScreenActivity.this.finish();
                startActivity(new Intent(WorkActivity.this , NotificationActivity.class));
            }
        });
        workDetailFargment fragobj3 = new workDetailFargment();
        fragobj3.setArguments(bundle3);
       // adapter.addFragment(fragobj, "Booked");

        adapter.addFragment(fragobj2, "Booked");
        adapter.addFragment(fragobj3, "Completed");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Log.e("error" , "  "+position);
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
