package com.example.project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.project.create_account_next.removeItem;

public class SkillsAdapter extends BaseAdapter {

   public static ArrayList<String> Skills ;
    Context con;
    public SkillsAdapter(Context con , ArrayList<String> Skills )
    {
        this.con = con;
        this.Skills = Skills;
    }
    @Override
    public int getCount() {
        return Skills.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view1 = LayoutInflater.from(con).inflate(R.layout.skill_item,parent,false);
        TextView skill = view1.findViewById(R.id.skill);
        skill.setText(Skills.get(position));
        ImageView remove = view1.findViewById(R.id.remove);
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Skills.remove(position);
                removeItem(position);
                notifyDataSetChanged();
            }
        });
        return view1;
    }
    public void AddItem(String skill)
    {
        Skills.add(skill);
        notifyDataSetChanged();
    }
}
