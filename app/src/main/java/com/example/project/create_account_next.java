package com.example.project;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static com.example.project.Create_Account.hideKeyboard;

public class create_account_next extends AppCompatActivity {

    int SELECT_IMAGE =101;
    int check = 0;
    static ArrayList<Integer> ItemPosition = new ArrayList<>();
    EditText ed_dob,ed_address,goc_number,opl_number,company_name,insurance_number;
    Spinner sskill,equipment_prefer,prefer,experince;
    Button btn_next2;
    public static void removeItem(int p)
    {
        ItemPosition.remove(p);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_next);
        ed_address=findViewById(R.id.address2);
        goc_number=findViewById(R.id.goc_number);
        opl_number=findViewById(R.id.opl_number);
        company_name=findViewById(R.id.companyName);
        insurance_number=findViewById(R.id.insurance_number);
        equipment_prefer=findViewById(R.id.equipmentPreferred);
        prefer=findViewById(R.id.prefer);
        experince=findViewById(R.id.experience);

        final String items[] = getResources().getStringArray(R.array.skill);
        final SkillsAdapter adapter = new SkillsAdapter(this , new ArrayList<String>());
        final ListView list = findViewById(R.id.list);
        list.setAdapter(adapter);
        check = getIntent().getIntExtra("key" , 0);

        final Calendar myCalendar = Calendar.getInstance();

         sskill= (Spinner) findViewById(R.id.sskill);
       sskill.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               boolean isAlreadyAdd = false;
               for(int i =0 ; i< SkillsAdapter.Skills.size() ; i++)
               {
                   if(SkillsAdapter.Skills.get(i).equalsIgnoreCase(items[position]))
                   {
                       isAlreadyAdd = true;
                       break;
                   }
               }
               if(isAlreadyAdd)
               {
                   Toast.makeText(create_account_next.this , "Skill Already Selected",Toast.LENGTH_LONG).show();
                   return;
               }
               ItemPosition.add(position);
               int i = position;
              // i--;
               if(i<=0)
                   return;
               Toast.makeText(create_account_next.this , "Select image for skill verification",Toast.LENGTH_LONG).show();
               selectimage();
               adapter.AddItem(items[i]);
               setListViewHeightBasedOnChildren(list);
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });
         ed_dob = (EditText) findViewById(R.id.dob);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "MM/dd/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                ed_dob.setText(sdf.format(myCalendar.getTime()));
            }

        };

        ed_dob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(create_account_next.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



         btn_next2 = findViewById(R.id.btn_next2);
        if(check == 1)
            btn_next2.setText("Save");
        btn_next2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();

            }
        });
        RelativeLayout rtl = findViewById(R.id.rtl);
        rtl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(create_account_next.this);
            }
        });
        TextView upload_picture = findViewById(R.id.upload_picture);
        upload_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectimage();
            }
        });


        TextView upload_proof = findViewById(R.id.upload_proof);
        upload_proof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectimage();
            }
        });

        TextView upload_proof2 = findViewById(R.id.upload_proof2);
        upload_proof2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectimage();
            }
        });

        TextView upload_proof3 = findViewById(R.id.upload_proof3);
        upload_proof3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectimage();
            }
        });


    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    void selectimage()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_IMAGE);
    }
    public void sendData(){
        try {
            Toast.makeText(getApplicationContext(),"called",Toast.LENGTH_LONG).show();
//            progressBar.show(this,"Loading..");
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = "http://134.209.29.174:3000/api/v1/locom-profile";

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("user_id", MyUser.getUser_id());
            jsonBody.put( "date_of_birth", ed_dob.getText().toString());
            jsonBody.put( "address", ed_address.getText().toString());
            jsonBody.put( "goc_number", goc_number.getText().toString());
            jsonBody.put( "opl_number", opl_number.getText().toString());
            jsonBody.put( "insurance_company", company_name.getText().toString());
            jsonBody.put( "insurance_no", insurance_number.getText().toString());
            jsonBody.put( "profile_photo", "");
            jsonBody.put( "equipment_preferred", String.valueOf(equipment_prefer.getSelectedItem()));
            JSONArray skillsarray=new JSONArray();

            for(int i=0;i<2;i++){
                JSONObject obj=new JSONObject();
//                ":[{“user_id":"147","skill_name ":"Test"},{
                try {
                    obj.put("user_id",MyUser.getUser_id());
                    obj.put("skill_name","Test");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                skillsarray.put(obj);
            }
            jsonBody.put( "skills", skillsarray);
            jsonBody.put( "year_of_experience", String.valueOf(experince.getSelectedItem()));
            jsonBody.put( "preferred_testing_time", String.valueOf(prefer.getSelectedItem()));


            JsonObjectRequest jobReq = new JsonObjectRequest(Request.Method.POST, URL, jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            try {
//                            Toast.makeText(getApplicationContext(),"response",Toast.LENGTH_LONG).show();
                                Log.d("Responses", jsonObject.toString());
                                int statuscod=jsonObject.getInt("status");
                                Log.i("Responses",statuscod+" this cod");
                                if(statuscod==200){

                                    JSONObject jsonObject1=jsonObject.getJSONObject("data");
                                    Log.d("Responses2", jsonObject1.toString());
                                    if(check == 1)
                                    {
                                        create_account_next.this.finish();
                                        return;
                                    }
                                    startActivity(new Intent(create_account_next.this , DashboardActivity.class));
////                                MyUser.setId(jsonObject1.getInt("id"));
//                                    String email=jsonObject1.getString("email");
//                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("email", email).apply();
//                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("password", passwprd.getText().toString()).apply();
//                                    Intent intent = new Intent(SignUp.this , CodeActivity.class);
//                                    startActivity(intent);

//                               Toast.makeText(getApplicationContext(),"number added "+mobnumber,Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("responses", volleyError.toString());
//                            progressBar.getDialog().dismiss();
                            Toast.makeText(getApplicationContext(),"try again",Toast.LENGTH_LONG).show();
                        }
                    });

//            requestQueue.add(jobReq);
            Volley.newRequestQueue(this).add(jobReq);
        } catch (JSONException e) {
            e.printStackTrace();
//            progressBar.getDialog().dismiss();
            Toast.makeText(getApplicationContext(),"some thing went Wrong",Toast.LENGTH_LONG).show();
        }
    }
}
