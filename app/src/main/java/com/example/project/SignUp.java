package com.example.project;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.project.utils.AppProperties;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.project.Create_Account.hideKeyboard;
import static com.example.project.utils.CommonMethods.isEmpty;
import static com.example.project.utils.Constants.USER_EMAIL;

public class SignUp extends AppCompatActivity {

    EditText firstname,lastname,email,phone,passwprd,confirmpassword;
    String s_firstname,s_lastname,s_email,s_phone,s_passwprd,s_confirmpassword;
    CheckBox ch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        firstname=findViewById(R.id.firstName);
        lastname=findViewById(R.id.lastName);
        email=findViewById(R.id.email2);
        phone=findViewById(R.id.phone);
        passwprd=findViewById(R.id.password);
        confirmpassword=findViewById(R.id.confirm_password);


        ch=findViewById(R.id.chbox);
        Button btn_signUp = findViewById(R.id.btn_signUp);
        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                s_firstname = firstname.getText().toString();
                s_lastname = lastname.getText().toString();
                s_email = email.getText().toString();
                s_phone = phone.getText().toString();
                s_passwprd = passwprd.getText().toString();
                s_confirmpassword = confirmpassword.getText().toString();
                if(!ch.isChecked()){
                    Toast.makeText(getApplicationContext(),"First agree to terms",Toast.LENGTH_LONG).show();
                    return;
                }

                if(isEmpty(s_firstname))
                {
                    firstname.setError("This field cannot be empty.");
                    return;
                }
                if(isEmpty(s_lastname))
                {
                    lastname.setError("This field cannot be empty.");
                    return;
                }
                if(isEmpty(s_email))
                {
                    email.setError("This field cannot be empty.");
                    return;
                }
                if(isEmpty(s_phone))
                {
                    phone.setError("This field cannot be empty.");
                    return;
                }
                if(isEmpty(s_passwprd))
                {
                    passwprd.setError("This field cannot be empty.");
                    return;
                }
                if(isEmpty(s_confirmpassword))
                {
                    confirmpassword.setError("This field cannot be empty.");
                    return;
                }
                if(!passwprd.getText().toString().equals(confirmpassword.getText().toString())){
                    Toast.makeText(getApplicationContext(),"Password is not matched",Toast.LENGTH_LONG).show();
                    return;
                }

                sendData();


            }
        });
        TextView login = findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent ;
                    intent = new Intent(SignUp.this , Login.class);


                startActivity(intent);

            }
        });
        RelativeLayout scroll = findViewById(R.id.rl);
        scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(SignUp.this);
            }
        });
        /*TextView login = findViewById(R.id.btn_signUp);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent ;

                   // intent = new Intent(SignUp.this , Login.class);

                //startActivity(intent);

            }
        });*/
    }


    public void sendData(){
        try {
            Toast.makeText(getApplicationContext(),"called",Toast.LENGTH_LONG).show();
//            progressBar.show(this,"Loading..");
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = "http://134.209.29.174:3000/api/v1/register";

            final JSONObject jsonBody = new JSONObject();
            jsonBody.put("first_name", firstname.getText().toString());
            jsonBody.put( "last_name", lastname.getText().toString());
            jsonBody.put( "email", email.getText().toString());
            jsonBody.put( "password", passwprd.getText().toString());
            jsonBody.put( "phone_no", phone.getText().toString());
            jsonBody.put( "tc_box", "1");
            //Following checks have been implemented as values required in databse are opposite which been set
            //in the code so these checks have been implemented.
            if(Who.select==1){
                jsonBody.put( "user_type", 0);
            }else if(Who.select==0){
                jsonBody.put( "user_type", 1);
            }else{
                Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_LONG).show();
                return;
            }


            JsonObjectRequest jobReq = new JsonObjectRequest(Request.Method.POST, URL, jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            try {
//                            Toast.makeText(getApplicationContext(),"response",Toast.LENGTH_LONG).show();
                                Log.d("Responses", jsonObject.toString());
                                int statuscod=jsonObject.getInt("status");
                                Log.i("Responses",statuscod+" this cod");
                                if(statuscod==200){

                                    JSONObject jsonObject1=jsonObject.getJSONObject("data");
                                    Log.d("Responses2", jsonObject1.toString());
                                    MyUser.setUser_id(jsonObject1.getInt("user_id"));
                                    MyUser.setFirst_name(jsonObject1.getString("first_name"));
                                    MyUser.setLast_name(jsonObject1.getString("last_name"));
                                    MyUser.setEmail(jsonObject1.getString("email"));
                                    MyUser.setPhone_no(jsonObject1.getString("phone_no"));
                                    MyUser.setTc_box(jsonObject1.getString("tc_box"));
                                    MyUser.setUser_type(jsonObject1.getString("user_type"));
                                    AppProperties.RegisterationToken = jsonObject1.getString("token");
//                                    String email=jsonObject1.getString("email");
//                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("email", email).apply();
//                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("password", passwprd.getText().toString()).apply();
                                    Intent intent = new Intent(SignUp.this , CodeActivity.class);
                                    intent.putExtra("email",MyUser.getEmail());
                                    intent.putExtra(USER_EMAIL,s_email.toLowerCase());
                                    intent.putExtra("password",passwprd.getText().toString());
                                    startActivity(intent);

//                               Toast.makeText(getApplicationContext(),"number added "+mobnumber,Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("responses", volleyError.toString());
//                            progressBar.getDialog().dismiss();
                            Toast.makeText(getApplicationContext(),"try again",Toast.LENGTH_LONG).show();
                        }
                    });

//            requestQueue.add(jobReq);
            Volley.newRequestQueue(this).add(jobReq);
        } catch (JSONException e) {
            e.printStackTrace();
//            progressBar.getDialog().dismiss();
            Toast.makeText(getApplicationContext(),"some thing went Wrong",Toast.LENGTH_LONG).show();
        }
    }
}
