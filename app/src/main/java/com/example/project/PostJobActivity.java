package com.example.project;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.project.model.StoreModel;
import com.example.project.utils.AppProperties;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.project.utils.CommonMethods.isEmpty;
import static com.example.project.utils.CommonMethods.showError;

public class PostJobActivity extends AppCompatActivity {

    LinearLayout start_date;
    boolean b1 = false;
    boolean b2 = false;
    boolean b3 = false;
    boolean b4 = false;
    boolean b5 = false;
    boolean b6 = false;
    EditText et_start , et_title , dayRate , description;
    String s_Date ,s_title ,s_dayRate ,s_description;
    Spinner job;
    Calendar myCalendar;
    LinearLayout date_start;
    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            s_Date = dayOfMonth+"-"+monthOfYear+"-"+year;
           et_start.setText(s_Date);
        }

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        dayRate = findViewById(R.id.dayRate);
        job = findViewById(R.id.job);
        myCalendar = Calendar.getInstance();
        date_start = findViewById(R.id.date_start);
        date_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        et_start = findViewById(R.id.et_start);
        et_title = findViewById(R.id.et_title);
        description = findViewById(R.id.description);
        start_date = findViewById(R.id.start_date);
        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(PostJobActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               PostJobActivity.this.finish();
            }
        });

        Button button = findViewById(R.id.btn_next);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postJob();
            }
        });

        final ImageView i1 = findViewById(R.id.i1);
        LinearLayout l1 = findViewById(R.id.l1);
        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b1)
                    i1.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i1.setColorFilter(getResources().getColor(R.color.blue));
                b1 = !b1;
            }
        });

        final ImageView i2 = findViewById(R.id.i2);
        LinearLayout l2 = findViewById(R.id.l2);
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b2)
                    i2.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i2.setColorFilter(getResources().getColor(R.color.blue));
                b2 = !b2;
            }
        });

        final ImageView i3 = findViewById(R.id.i3);
        LinearLayout l3 = findViewById(R.id.l3);
        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b3)
                    i3.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i3.setColorFilter(getResources().getColor(R.color.blue));
                b3 = !b3;
            }
        });

        final ImageView i4 = findViewById(R.id.i4);
        LinearLayout l4 = findViewById(R.id.l4);
        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b4)
                    i4.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i4.setColorFilter(getResources().getColor(R.color.blue));
                b4 = !b4;
            }
        });

        final ImageView i5 = findViewById(R.id.i5);
        LinearLayout l5 = findViewById(R.id.l5);
        l5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b5)
                    i5.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i5.setColorFilter(getResources().getColor(R.color.blue));
                b5 = !b5;
            }
        });
        final ImageView i6 = findViewById(R.id.i6);
        LinearLayout l6 = findViewById(R.id.l6);
        l6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b6)
                    i6.setColorFilter(getResources().getColor(R.color.light_gray3));
                else
                    i6.setColorFilter(getResources().getColor(R.color.blue));
                b6 = !b6;
            }
        });
        i1.setColorFilter(getResources().getColor(R.color.light_gray3));
        i2.setColorFilter(getResources().getColor(R.color.light_gray3));
        i3.setColorFilter(getResources().getColor(R.color.light_gray3));
        i4.setColorFilter(getResources().getColor(R.color.light_gray3));
        i5.setColorFilter(getResources().getColor(R.color.light_gray3));
        i6.setColorFilter(getResources().getColor(R.color.light_gray3));



    }

    void postJob()
    {
        s_title = et_title.getText().toString();
        s_dayRate = dayRate.getText().toString();
        s_description = description.getText().toString();
        if(isEmpty(s_title))
        {
            et_title.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_dayRate))
        {
            dayRate.setError("This field cannot be empty.");
            return;
        }
        if(isEmpty(s_description))
        {
            et_title.setError("This field cannot be empty.");
            return;
        }


        Thread networkThread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                    RequestBody body = RequestBody.create(mediaType, "start_date="+et_start.getText().toString()+"&end_date=2019-01-31&country=pakistan&distance=2&testing_time=4%20PM&min_weekday_amount=56&min_weekend_amount=5&job_id=BCD-0001&no_posts=5&perday_amount="+dayRate.getText().toString()+"&skills="+job.getSelectedItem()+"&job_detail="+description.getText().toString()+"&address=aaa&parking="+(b1 ? "1" : "0")+"&pre_test="+(b2 ? "1" : "0")+"&field_test="+(b3 ? "1" : "0")+"&phorotoper="+(b4 ? "1" : "0")+"&trail_frame="+(b5 ? "1" : "0")+"&store_id=1&job_title="+et_title.getText().toString());
                    Request request = new Request.Builder()
                            .url("http://134.209.29.174:3000/api/v1/jobs")
                            .post(body)
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InV1bWFpcjYxOUBnbWFpbC5jb20iLCJpZCI6MTUsImlhdCI6MTU1NTM5NjEyNywiZXhwIjoxNTU1NDcxNzI3fQ.u0s4jL6PDtNvLKE6pMW3asWVxcUg4d-4IzTNXfkeRIA")
                            .addHeader("cache-control", "no-cache")
                            .addHeader("Postman-Token", "52fd8004-6278-4f54-8bb4-999e0b5a8f6d")
                            .build();

                    Response response = client.newCall(request).execute();
                    if(response.code() == 200)
                    {
                        String JsonResponse = response.body().string().toString();
                        JSONObject object = new JSONObject(JsonResponse);
                        if(object.getInt("status") == 200)
                        {
                            /*JSONObject data = object.getJSONObject("data");
                            Gson gson =  new Gson();
                            StoreModel model = gson.fromJson(data.toString() , StoreModel.class);
                            AppProperties.LoginStore = model;
                            startActivity(new Intent(PostJobActivity.this , JobFeedActivity.class));*/

                        }
                        else
                        {
                            showError(PostJobActivity.this , "Status Code is not 200");
                        }
                    }
                    else
                    {
                        showError(PostJobActivity.this , "Some error occur please check your internet");
                    }
                }
                catch (Exception e)
                {

                }

            }
        });
        networkThread.start();


    }
}
